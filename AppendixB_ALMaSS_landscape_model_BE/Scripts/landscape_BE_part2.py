#=======================================================================================================================
# Name: Landscape generator for Poland, part2  -  landscape_PL_part2
# Purpose: The script cleans up the raw landscape map from sliver polygons and creats ALMaSS input landscape map.
# Authors: Elzbieta Ziolkowska - June 2017
# Last large update: Sept 2018

#===== Chunk: Setup =====#
# Import system modules
import arcpy, traceback, sys, time, gc, shutil, os, csv
from dbfpy import dbf
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
nowTime = time.strftime('%X %x')
gc.enable
print "Model landscape generator part2 started: " + nowTime
print "... system modules checked"

# Data - paths to data, output gdb, scratch folder and simulation landscape mask

#test_areas = ["test_area2", "test_area3", "test_area4", "test_area5", "test_area6", "test_area7", "test_area8", "test_area10"]
test_areas = ["Merendree"]
for test_area in test_areas:
    outPath = "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS\output Merendree.gdb/" 
    localSettings = "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS\Merendree.gdb""/" + test_area 
    gisDB = "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS\GIS geodatabase.gdb""/"
    scratchDB = "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS\scratch Merendree.gdb/"
    asciiexp = "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS\ASCII_Merendree.txt"
    reclasstable = "C:\Users\dclaeysb\documenten\landscape generation Almass\OutputMerendree\reclass_new.txt"
    attrexp = "C:\Users\dclaeysb\documenten\landscape generation Almass\OutputMerendree\ATTR_Merendree.csv" 

    # Model settings
    arcpy.env.overwriteOutput = True
    arcpy.env.workspace = gisDB
    arcpy.env.scratchWorkspace = scratchDB
    arcpy.env.extent = localSettings
    arcpy.env.mask = localSettings
    arcpy.env.cellSize = localSettings
    print "... model settings read"

    # Defining dbf to csv conversion function
    def dbf_to_csv(out_table):#Input a dbf, output a csv
        csv_fn = out_table[:-4]+ ".csv" #Set the table as .csv format
        with open(csv_fn,'wb') as csvfile: #Create a csv file and write contents from dbf
            in_db = dbf.Dbf(out_table)
            out_csv = csv.writer(csvfile)
            names = []
            for field in in_db.header.fields: #Write headers
                names.append(field.name)
            out_csv.writerow(names)
            for rec in in_db: #Write records
                out_csv.writerow(rec.fieldData)
            in_db.close()
        return csv_fn

    print " "
    #===== End chunk: setup =====#

    #####################################################################################################

    try:
    #===== Chunk: Elimination of sliver polygons =====#
        print "Eliminating sliver polygons ..."

    #step0
        print "Step 0: Removing water from the process (including unsprayed water buffer zone :: 100) ..."
        map0_NW = SetNull(outPath + "MapRaw", outPath + "MapRaw", "VALUE IN (100, 110, 112, 113, 114, 115, 116, 120, 125)")
        map0_NW.save(scratchDB + "map0_NW")
        print "Step 0 finished ..."

    #step1: small (<50 cells) sliver polygons eliminated for all besides buildings (450), industrial chimneys (441), transmission towers (231), wind turbines (232), wind mills (233), high voltage masts (234), trees (321), and field boundaries (900)
        print "Step 1: Remowing sliver polygons <50 cells ..."
        con = (map0_NW<440)&(map0_NW>442)
        toCheck = Con(con, map0_NW, 0)
        regGr = RegionGroup(toCheck, "FOUR", "WITHIN", "", 0)
        toNibble = SetNull(regGr, 1, "COUNT < 50")
        map1_NW = Nibble(map0_NW, toNibble, "DATA_ONLY")
        map1_NW.save(scratchDB + "map1_NW")
        #arcpy.RasterToPolygon_conversion(map1_NW, scratchDB + "map1_NW_vector2", "NO_SIMPLIFY", "VALUE")
        print "Step 1 finished ..."

        map1_NW = map0_NW
        map1_NW.save(scratchDB + "map1_NW")
        arcpy.RasterToPolygon_conversion(map1_NW, scratchDB + "map1_NW_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 1 finished ..."

 
    #step2: checking if any big background polygons (> 1ha) exists and if so convert them into fields
        print "Step 2: Checking for big (>1ha) background polygons ..."
        arcpy.Select_analysis(agriculture, scratchDB + "arable land", "gridcode = 1")
        if int(arcpy.GetCount_management(scratchDB + "arable_land").getOutput(0))>0: 
            if arcpy.Exists(outPath + "NA520"):
                arcpy.Delete_management(outPath + "NA520")
                print "... deleting existing raster"
        arcpy.PolygonToRaster_conversion(scratchDB + "arable_land", "tdnCode", scratchDB + "tmpRaster", "CELL_CENTER", "NONE", "1")
        outCon = Con(IsNull(Raster(scratchDB + "tmpRaster")), 0, 1)

        map1_NW_MAXResult = arcpy.GetRasterProperties_management(scratchDB + "map1_NW", "MAXIMUM")
        map1_NW_MAX = map1_NW_MAXResult.getOutput(0)

        arcpy.MakeFeatureLayer_management(scratchDB + "map1_NW_vector", "map1_NW_lyr")
        arcpy.SelectLayerByAttribute_management("map1_NW_lyr", "NEW_SELECTION", "gridcode = 1")
        arcpy.CopyFeatures_management("map1_NW_lyr", scratchDB + "map1_NW_selection")
        arcpy.SelectLayerByAttribute_management("map1_NW_lyr", "SUBSET_SELECTION", "(Shape_Area >= 10000) AND (Shape_Area/Shape_Length>5)")
        arcpy.CopyFeatures_management("map1_NW_lyr", scratchDB + "map1_NW_selection2")

        if int(arcpy.GetCount_management(scratchDB + "map1_NW_selection2").getOutput(0))>0:
            ZonalStatisticsAsTable(scratchDB + "map1_NW_selection2", "Id", outCon, scratchDB + "map1_NW_table", "NODATA", "ALL")
            arcpy.JoinField_management (scratchDB + "map1_NW_vector", "Id", scratchDB + "map1_NW_table", "Id", ["MEAN"])
            cursor = arcpy.UpdateCursor(scratchDB + "map1_NW_vector")
            i = 1
            for row in cursor:
                if row.getValue('MEAN')>=0.8:
                    print int(map1_NW_MAX) + i
                    row.setValue('gridcode', int(map1_NW_MAX) + i)   # setting gridcode for a new field
                    i = i+1
                cursor.updateRow(row)#

    # Updating field1000 layer and adding info on farm type (all new fields belongs to one big cattle farm, as cattle farms are the most common ones)\
        arcpy.Update_analysis(scratchDB + "fields1000", scratchDB + "map1_NW_selection2", scratchDB + "fields_and_farms", "BORDERS")
        cursor = arcpy.UpdateCursor(scratchDB + "fields_and_farms")
        j=1
        for row in cursor:
           if row.getValue('FARM_TYPE')=='':
                row.setValue('FARM_TYPE', "Cattle farms")   # setting gridcode for a new field
                row.setValue('gridcode', int(map1_NW_MAX) + j)   # setting gridcode for a new field
                j=j+1
                cursor.updateRow(row)
        # Exporting field1000 to shp in final output location
        arcpy.FeatureClassToShapefile_conversion(scratchDB + "fields_and_farms", "C:\Users\dclaeysb\documenten\landscape generation Almass\Output"+ test_area + " - kopie" )
        dbf_to_csv("C:\Users\dclaeysb\documenten\landscape generation Almass\Output"+ test_area + " - kopie""/""fields_and_farms.dbf")


        arcpy.PolygonToRaster_conversion(scratchDB + "map1_NW_vector", "gridcode", scratchDB + "map2_NW", "", "", 1)
        arcpy.Delete_management(outPath + "tmpRaster")
        print "Step 2 finished ..."

    #step3: removing elongated background polygons
        print "Step 3: Removing elongated background polygons ..."
        arcpy.MakeFeatureLayer_management(scratchDB + "map1_NW_vector", "map2_NW_lyr")
        arcpy.SelectLayerByAttribute_management("map2_NW_lyr", "NEW_SELECTION", "gridcode = 1")
        arcpy.CopyFeatures_management("map2_NW_lyr", scratchDB + "map2_NW_selection")
        
        arcpy.MinimumBoundingGeometry_management(scratchDB + "map2_NW_selection", scratchDB + "map2_NW_selection_bounding", "CIRCLE", "NONE", "", "MBG_FIELDS")
        arcpy.JoinField_management(scratchDB + "map2_NW_selection", "Id", scratchDB + "map2_NW_selection_bounding", "Id", "")
        arcpy.MakeFeatureLayer_management(scratchDB + "map2_NW_selection", "map2_NW_selection_lyr")
        arcpy.SelectLayerByAttribute_management("map2_NW_selection_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 < 0.05 AND Shape_Area/ Shape_Length<4 AND Shape_Area < 1500")
        arcpy.PolygonToRaster_conversion("map2_NW_selection_lyr", "gridcode", scratchDB + "map2_NW_selection_lyr_raster", "", "", 1)
        Reclass2 = Con(IsNull(scratchDB + "map2_NW_selection_lyr_raster"), 2, 1)
        toNibble2 = SetNull(Reclass2, 2, "Value = 1")
        map3_NW = Nibble(scratchDB + "map2_NW", toNibble2, "DATA_ONLY")
        map3_NW.save(scratchDB + "map3_NW")
        arcpy.RasterToPolygon_conversion(scratchDB + "map3_NW", scratchDB + "map3_NW_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 3 finished ..."

    #step4: removing background polygons smaller than 500 cells
        print "Step 4: Removing background polygons <500 cells ..."
        toCheck3 = Con(map3_NW==1, map3_NW, 0)
        regGr3 = RegionGroup(toCheck3, "FOUR", "WITHIN", "", 0)
        toNibble3 = SetNull(regGr3, 1, "COUNT < 500")
        map4_NW = Nibble(map3_NW, toNibble3, "DATA_ONLY")
        map4_NW.save(scratchDB + "map4_NW")
        arcpy.RasterToPolygon_conversion(map4_NW, scratchDB + "map4_NW_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 4 finished ..."

        map3_NW.save(scratchDB + "map4_NW")
        arcpy.RasterToPolygon_conversion(scratchDB + "map4_NW", scratchDB + "map4_NW_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 4 finished ..."

    #step5: removing small parts of multi-divided fields
        print "Step 5: Checking for small parts of multi-divided fields ..."
        cursor=arcpy.SearchCursor(scratchDB + "map4_NW_vector")
        fields_list=[]
        duplicates=[]
        for row in cursor:
          a=row.getValue("gridcode")
          if a>=1000:
            if a in fields_list:
              duplicates.append(a)
            else:
              fields_list.append(a)

        if len(duplicates)>0:
            print "multi-divided fields found ..."
            whereClause = "VALUE IN (" + str(duplicates)[1:-1] + ")"
            toCheck4 = Con(scratchDB + "map4_NW", scratchDB + "map4_NW", 0, whereClause)
            regGr4 = RegionGroup(toCheck4, "FOUR", "WITHIN", "", 0)
            toNibble4 = SetNull(regGr4, 1, "COUNT < 1000")
            map5_NW = Nibble(scratchDB + "map4_NW", toNibble4, "DATA_ONLY")
            map5_NW.save(scratchDB + "map5_NW")
            arcpy.RasterToPolygon_conversion(map5_NW, scratchDB + "map5_NW_vector", "NO_SIMPLIFY", "VALUE")   
            print "small multi-divided fields removed ..."
        else:
            print "no multi-divided fields found ..."
            arcpy.CopyRaster_management(scratchDB + "map4_NW", scratchDB + "map5_NW")
            arcpy.CopyFeatures_management(scratchDB + "map4_NW_vector", scratchDB + "map5_NW_vector")
        print "Step 5 finished ..."

    #step6: adding the rivers back
        print "Step 6: Adding water ..."
        map6 = Con(IsNull(scratchDB + "map0_NW"), Raster(outPath + "MapRaw"), Raster(scratchDB + "map5_NW"))
        map6.save(scratchDB + "map6")
        # assigning riverside plants to all gaps, i.e., 'NODATA' values
        map6_fill = Con(IsNull(map6), 529, map6)
        map6_fill.save(scratchDB + "map6_fill")
        arcpy.RasterToPolygon_conversion(map6_fill, scratchDB + "map6_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 6 finished ..."

    #step7: filling the background gaps
        print "Step 7: Filling the remaining gaps ..."
        gridcodeValues = []
        cursor = arcpy.SearchCursor(scratchDB + "map6_vector")
        for row in cursor:
            gridcodeValues.append(row.getValue('gridcode'))

        if 1 in gridcodeValues:
            print "Still some background pixels left ..."
            arcpy.PolygonNeighbors_analysis(scratchDB + "map6_vector", scratchDB + "map6_vector_neighbors")
            arcpy.JoinField_management (scratchDB + "map6_vector_neighbors", "src_OBJECTID", scratchDB + "map6_vector", "OBJECTID", ["gridcode"])
            arcpy.TableSelect_analysis(scratchDB + "map6_vector_neighbors", scratchDB + "map6_vector_neighbors_selection", "gridcode = 1")
            arcpy.JoinField_management (scratchDB + "map6_vector_neighbors_selection", "nbr_OBJECTID", scratchDB + "map6_vector", "OBJECTID", ["gridcode"])
            arcpy.TableSelect_analysis(scratchDB + "map6_vector_neighbors_selection", scratchDB + "map6_vector_neighbors_selection2", "gridcode_1 >= 411 AND gridcode_1 <= 422")
            arcpy.JoinField_management (scratchDB + "map6_vector", "OBJECTID", scratchDB + "map6_vector_neighbors_selection2", "src_OBJECTID")
            cursor = arcpy.UpdateCursor(scratchDB + "map6_vector")
            for row in cursor:
                if row.getValue('gridcode')==1:
                    if row.getValue('src_OBJECTID')>0:
                        row.setValue('gridcode', 570)   # setting category 'areas with bulildings on countryside' to selected background objects
                    else:
                        row.setValue('gridcode', 570)   # setting category 'wasteland' to the rest of 'background' objects
                cursor.updateRow(row)
        else:
            print "No background pixels found ..."
        arcpy.PolygonToRaster_conversion(scratchDB + "map6_vector", "gridcode", scratchDB + "map7", "", "", 1)
        map7 = Raster(scratchDB + "map7")
        print "Step 7 finished ..."

    #step8: Cleaning of elongated sliver field polygons, e.g. left overs located between line of trees/hedgerow and road verge
        print "Step 8: Cleaning of elongated sliver field polygons ..."
        arcpy.MakeFeatureLayer_management(scratchDB + "map6_vector", "map6_lyr")
        arcpy.SelectLayerByAttribute_management("map6_lyr", "NEW_SELECTION", "gridcode >= 1000")
        arcpy.CopyFeatures_management("map6_lyr", scratchDB + "map6_selection")
        arcpy.MinimumBoundingGeometry_management(scratchDB + "map6_selection", scratchDB + "map6_selection_bounding", "CIRCLE", "NONE", "", "MBG_FIELDS")
        arcpy.JoinField_management(scratchDB + "map6_selection", "Id", scratchDB + "map6_selection_bounding", "Id", "")
        arcpy.MakeFeatureLayer_management(scratchDB + "map6_selection", "map6_selection_lyr")
        arcpy.SelectLayerByAttribute_management("map6_selection_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 < 0.1 AND Shape_Area/Shape_Length<4")
        arcpy.CopyFeatures_management("map6_selection_lyr", scratchDB + "map6_selection2")
        if int(arcpy.GetCount_management(scratchDB + "map6_selection2").getOutput(0))>0:
            arcpy.PolygonToRaster_conversion(scratchDB + "map6_selection2", "gridcode", scratchDB + "map6_selection2_raster", "", "", 1)
            Reclass7 = Con(IsNull(scratchDB + "map6_selection2_raster"), 2, 1)
            toNibble7 = SetNull(Reclass7, 2, "Value = 1")
            map8 = Nibble(map7, toNibble7, "DATA_ONLY")
            map8.save(scratchDB + "map8")
            arcpy.RasterToPolygon_conversion(map8, scratchDB + "map8_vector", "NO_SIMPLIFY", "VALUE")
        else:
            print "no cleaning needed ..."
            arcpy.CopyRaster_management(scratchDB + "map7", scratchDB + "map8")
        print "Step 8 finished ..."

    #step9: final cleaning of small (<50 cells) sliver polygons eliminated for all besides water theme (110-150), buildings (450), industrial chimneys (441), transmission towers (231), wind turbines (232),
    # wind mills (233), high voltage masts (234), trees (321), river plants (520), and field boundaries (900)
        print "Step 9: Final cleaning of sliver polygons <50 cells ..."
        con8 = (map8<100)|((map8>200) & (map8<300))|((map8>500) & (map8<600))
        toCheck8 = Con(con8, map8, 0)
        regGr8 = RegionGroup(toCheck8, "FOUR", "WITHIN", "", 0)
        toNibble8 = SetNull(regGr8, 1, "COUNT < 50")
        map9 = Nibble(map8, toNibble8, "DATA_ONLY")
        map9.save(scratchDB + "map9")

        map9 = map8
        map9.save(scratchDB + "map9")
      
        arcpy.RasterToPolygon_conversion(map9, scratchDB + "map9_vector", "NO_SIMPLIFY", "VALUE")

        cursor=arcpy.SearchCursor(scratchDB + "map9_vector")
        fields_list=[]
        duplicates_left=[]
        for row in cursor:
          a=row.getValue("gridcode")
          if a>=1000:
            if a in fields_list:
              duplicates_left.append(a)
            else:
              fields_list.append(a)


        arcpy.Dissolve_management(scratchDB + "map9_vector", scratchDB + "map9_vector_dissolve", ["gridcode"], "", "", "")
        arcpy.Update_analysis(scratchDB + "map9_vector", scratchDB + "map9_vector_dissolve", scratchDB + "map_final_vector", "BORDERS")
        arcpy.PolygonToRaster_conversion(scratchDB + "map_final_vector", "gridcode", outPath + "map_final", "", "", 1)
        print "Step 9 finished ..."
        nowTime = time.strftime('%X %x')
        print "Sliver polygons elimination done ..." + nowTime

        #===== Chunk: Finalize =====#
        # Reclassify to ALMaSS raster values
        # ALMaSS uses different values for the landcover types, so this step simply translates
        # the numeric values.
        reclass_land = ReclassByASCIIFile(outPath + "map_final", reclasstable, "DATA")
        reclass_land.save(outPath + "map_final_reclass")
        nowTime = time.strftime('%X %x')
        print "Reclassification done ..." + nowTime

        # regionalise map
        regionALM = RegionGroup(reclass_land,"EIGHT","WITHIN","ADD_LINK","")
        regionALM.save(outPath + "Map_ALMaSS")
        nowTime = time.strftime('%X %x')
        print "Regionalisation done ..." + nowTime

    ### export attribute table 
        table = outPath + "Map_ALMaSS"
        # Write an attribute tabel - based on this answer:
        # https://geonet.esri.com/thread/83294
        # List the fields
        fields = arcpy.ListFields(table)  
        field_names = [field.name for field in fields]  
          
        with open(attrexp,'wb') as f:  
            w = csv.writer(f)  
            # Write the headers
            w.writerow(field_names)  
            # The search cursor iterates through the 
            for row in arcpy.SearchCursor(table):  
                field_vals = [row.getValue(field.name) for field in fields]  
                w.writerow(field_vals)  
                del row
        print "Attribute table exported..." + nowTime   

        # convert regionalised map to ascii
        arcpy.RasterToASCII_conversion(regionALM, asciiexp)
        print "Conversion to ASCII done ..." + nowTime

        endTime = time.strftime('%X %x')
        print ""
        print "Landscape generated: " + endTime
        #===== End Chunk: Finalize =====#

        gc.collect()

    except:
        if arcpy.Exists(outPath + "tmpRaster"):
          arcpy.Delete_management(outPath + "tmpRaster")
        tb = sys.exc_info()[2]
        tbinfo = traceback.format_tb(tb)[0]
        pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " +        str(sys.exc_type) + ": " + str(sys.exc_value) + "\n"
        msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

        arcpy.AddError(msgs)
        arcpy.AddError(pymsg)

        print msgs
        print pymsg

    arcpy.AddMessage(arcpy.GetMessages(1))
    print arcpy.GetMessages(1)
