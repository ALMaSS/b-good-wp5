#=======================================================================================================================
# Name: Landscape generator for the Netherlands
# Purpose: The script convert feature layers to rasters and assemble a raw surface covering land-use map. Further analysis, including cleaning of sliver polygons and creating ALMaSS input landscape map are covered by separate script due to processing issues.
# Authors: original script for Denmark by Flemming Skov & Lars Dalby - Oct-Dec 2014; modified for Poland by Elzbieta Ziolkowska - Feb 2017, modified for Belgium by David Claeys Bouuaert - december 2019
# Last large update: december 2019

#===== Chunk: Setup =====#
# Import system modules
import arcpy, traceback, sys, time, gc, os
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
nowTime = time.strftime('%X %x')
gc.enable
print ("Model landscape generator started: ") + nowTime
print ("... system modules checked")

# Data - paths to data, output gdb, scratch folder and simulation landscape mask
test_area = "Merendree"
nationalDB = "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS\NationalDB2.gdb"
outPath = "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS\output " + test_area + ".gdb/"
localSettings = "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS/" + test_area + ".gdb/" + test_area
gisDB = "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS\GIS geodatabase" + test_area + ".gdb""/"
scratchDB = "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS\scratch " + test_area + ".gdb/"



# Model settings
arcpy.env.overwriteOutput = True
arcpy.env.workspace = gisDB
arcpy.env.scratchWorkspace = scratchDB
arcpy.env.extent = localSettings
arcpy.env.mask = localSettings
arcpy.env.cellSize = localSettings
print ("... model settings read")

# Model execution - controls which processes to run:
default = 1  # 1 -> run process; 0 -> do not run process

# Mosaic
road = default      #create road theme
builtup = default #create built up theme
nature = default       #create nature theme
water = default   #create fresh water theme
cultural = default      #create cultural feature theme
cultivable = default #create cultivable land theme
finalmap = default      #assemble final map

print (" ")
#===== End chunk: setup =====#

#####################################################################################################

try:

#===== Chunk: Clipping =====#
# from national data to a given study area

  print ("Clipping national data to the study area extent")

  for root, dirs, datasets in arcpy.da.Walk(nationalDB):
    for ds in datasets:
      print ("Clipping ") + ds + " ..."
      arcpy.Clip_analysis(os.path.join(root, ds), localSettings, gisDB + ds, "")

#===== End: Clipping =====#

#===== Chunk: Conversion =====#
# from feature layers to raster


# Check which layers exists in folder "D:/Dutch_project/landscape_models/test_areas/" + test_area + "/" + test_area + ".gdb"
  if int(arcpy.GetCount_management(gisDB + "Nature_reserve").getOutput(0))>0:
    Nature_reserve = 1
  else:
    Nature_reserve = 0
    print ("Nature_reserve layer empty")
  if int(arcpy.GetCount_management(gisDB + "rail_line").getOutput(0))>0:
    rail_line = 1
  else:
    rail_line = 0
    print ("rail_line layer empty")
  if int(arcpy.GetCount_management(gisDB + "roads_line").getOutput(0))>0:
    roads_line = 1   #bike paths and pedestrians
  else:
    roads_line = 0
    print "Road_line layer empty"
  if int(arcpy.GetCount_management(gisDB + "tree_point").getOutput(0))>0:
    tree_point = 1
  else:
    tree_point = 0
    print "tree_point layer empty"
  if int(arcpy.GetCount_management(gisDB + "water_area").getOutput(0))>0:
    water_area = 1
  else:
    water_area = 0
    print "water_area layer empty"
  if int(arcpy.GetCount_management(gisDB + "water_line").getOutput(0))>0:
    water_line = 1
  else:
    water_line = 0
    print "water_line layer empty"
  if int(arcpy.GetCount_management(gisDB + "buildings").getOutput(0))>0:
    buildings = 1
  else:
    buildings = 0
    print "Buildings layer empty"
  if int(arcpy.GetCount_management(gisDB + "orchard").getOutput(0))>0:
    orchard = 1
  else:
    orchard = 0
    print "orchard layer empty"
  if int(arcpy.GetCount_management(gisDB + "agriculture").getOutput(0))>0:
    agriculture = 1
  else:
    agriculture = 0
    print "Infra_line layer empty"
  if int(arcpy.GetCount_management(gisDB + "natural").getOutput(0))>0:
    natural = 1
  else:
    natural = 0
    print "natural layer empty"

# land
  arcpy.PolygonToRaster_conversion(localSettings, "OBJECTID", outPath + "land", "CELL_CENTER", "", 1)


## THEME WATER (1)
  print "Processing WATER THEME..."

  rasterList_water = []
  rasterList_buffer_water_zone = []

# 113:115 -  water_line   
  if water_line == 1:
    print "Processing rivers..."
    arcpy.MakeFeatureLayer_management("water_line", "river_layer", "", "", "")
    arcpy.CopyFeatures_management("river_layer", scratchDB + "river_selection")

    print "stream..."
    arcpy.Select_analysis(scratchDB + "river_selection",scratchDB + "stream", """"Code" = 113""")
    if int(arcpy.GetCount_management(scratchDB + "stream").getOutput(0))>0:
      if arcpy.Exists(outPath + "WA113"):
        arcpy.Delete_management(outPath + "WA113")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "stream","","1","")
      rasTemp = Con(eucDistTemp < 3, 113, 1)
      rasTemp.save(outPath + "WA113")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "WA113")
    rasterList_water.append(outPath + "WA113")
    rasterList_buffer_water_zone.append(outPath + "WA113")

    print "river..."
    arcpy.Select_analysis(scratchDB + "river_selection",scratchDB +  "river", """"Code" = 114""")
    if int(arcpy.GetCount_management(scratchDB + "river").getOutput(0))>0:
      if arcpy.Exists(outPath + "WA114"):
        arcpy.Delete_management(outPath + "WA114")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "river","","1","")
      rasTemp = Con(eucDistTemp < 40, 114, 1)
      rasTemp.save(outPath + "WA114")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "WA114")
    rasterList_water.append(outPath + "WA114")
    rasterList_buffer_water_zone.append(outPath + "WA114")
    
    print "canal..."
    arcpy.Select_analysis(scratchDB + "river_selection",scratchDB +  "canal", """"Code" = 115""")
    if int(arcpy.GetCount_management(scratchDB + "canal").getOutput(0))>0:
      if arcpy.Exists(outPath + "WA115"):
        arcpy.Delete_management(outPath + "WA115")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "canal","","1","")
      rasTemp = Con(eucDistTemp < 50, 115, 1)
      rasTemp.save(outPath + "WA115")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "WA115")
    rasterList_water.append(outPath + "WA115")
    rasterList_buffer_water_zone.append(outPath + "WA115")

    print "drain..."
    arcpy.Select_analysis(scratchDB + "river_selection",scratchDB + "drain", """"Code" = 112""")
    if int(arcpy.GetCount_management(scratchDB + "drain").getOutput(0))>0:
      if arcpy.Exists(outPath + "WA112"):
        arcpy.Delete_management(outPath + "WA112")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "drain","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 112, 1)
      rasTemp.save(outPath + "WA112")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "WA112")
    rasterList_water.append(outPath + "WA112")
    rasterList_buffer_water_zone.append(outPath + "WA112")

# 110+116-125 - water_area
  if water_area == 1:
    print "Processing rivers and lakes from water_area layer ..."

    print "water ..."
    arcpy.Select_analysis("water_area", scratchDB + "water", """"Code" = 110""")
    if int(arcpy.GetCount_management(scratchDB + "water").getOutput(0))>0:
      if arcpy.Exists(outPath + "WA110"):
        arcpy.Delete_management(outPath + "WA110")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "water", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 110)
      rasTemp.save(outPath + "WA110")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "WA110")
    rasterList_water.append(outPath + "WA110")

    print "river2 ..."
    arcpy.Select_analysis("water_area", scratchDB + "river", """"Code" = 116""")
    if int(arcpy.GetCount_management(scratchDB + "river").getOutput(0))>0:
      if arcpy.Exists(outPath + "WA116"):
        arcpy.Delete_management(outPath + "WA116")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "river", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 116)
      rasTemp.save(outPath + "WA116")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "WA116")
    rasterList_water.append(outPath + "WA116")
    rasterList_buffer_water_zone.append(outPath + "WA116")
    
    print "dock ..."
    arcpy.Select_analysis("water_area", scratchDB + "dock", """"Code" = 120""")
    if int(arcpy.GetCount_management(scratchDB + "dock").getOutput(0))>0:
      if arcpy.Exists(outPath + "WA120"):
        arcpy.Delete_management(outPath + "WA120")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "dock", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 120)
      rasTemp.save(outPath + "WA120")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "WA120")
    rasterList_water.append(outPath + "WA120")

    print "reservoir ..."
    arcpy.Select_analysis("water_area", scratchDB + "reservoir", """"Code" = 125""")
    if int(arcpy.GetCount_management(scratchDB + "reservoir").getOutput(0))>0:
      if arcpy.Exists(outPath + "WA125"):
        arcpy.Delete_management(outPath + "WA125")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "reservoir", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 125)
      rasTemp.save(outPath + "WA125")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "WA125")
    rasterList_water.append(outPath + "WA125")
    rasterList_buffer_water_zone.append(outPath + "WA125")

    # Creating 1-m buffer(unsprayed) zones around all water
    print rasterList_buffer_water_zone
    maxRaster = CellStatistics(rasterList_buffer_water_zone, "MAXIMUM", "DATA")
    outSetNull = SetNull(maxRaster, 1, "VALUE = 1")
    eucDistTemp = EucDistance(outSetNull,"","1","")
    rasTemp = Con((eucDistTemp < 1.50) & (eucDistTemp > 0), 100, 1)
    rasTemp.save(outPath + "WA100")
    rasterList_water.append(outPath + "WA100")  

## THEME ROADS (2)
  print "Processing ROAD THEME ..."

  rasterList_road = []

# 201-235 - roads from road_line   
  if roads_line == 1:
    print "Processing roads..."
    arcpy.MakeFeatureLayer_management("roads_line", "road_layer", "", "", "")
    arcpy.CopyFeatures_management("road_layer", scratchDB + "road_selection")

    print "unknown..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "unknown", """"Code" = 201""")
    if int(arcpy.GetCount_management(scratchDB + "unknown").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD201"):
        arcpy.Delete_management(outPath + "RD201")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "unknown","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 201, 1)
      rasTemp.save(outPath + "RD201")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD201")
    rasterList_road.append(outPath + "RD201")

    print "unclassified..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "unclassified", """"Code" = 202""")
    if int(arcpy.GetCount_management(scratchDB + "unclassified").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD202"):
        arcpy.Delete_management(outPath + "RD202")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "unclassified","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 202, 1)
      rasTemp.save(outPath + "RD202")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD202")
    rasterList_road.append(outPath + "RD202")

    print "steps..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "steps", """"Code" = 203""")
    if int(arcpy.GetCount_management(scratchDB + "steps").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD203"):
        arcpy.Delete_management(outPath + "RD203")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "steps","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 203, 1)
      rasTemp.save(outPath + "RD203")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD203")
    rasterList_road.append(outPath + "RD203")

    print "bridleway..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "bridleway", """"Code" = 204""")
    if int(arcpy.GetCount_management(scratchDB + "bridleway").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD204"):
        arcpy.Delete_management(outPath + "RD204")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "bridleway","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 204, 1)
      rasTemp.save(outPath + "RD204")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD204")
    rasterList_road.append(outPath + "RD204")

    print "footway..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "footway", """"Code" = 205""")
    if int(arcpy.GetCount_management(scratchDB + "footway").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD205"):
        arcpy.Delete_management(outPath + "RD205")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "footway","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 205, 1)
      rasTemp.save(outPath + "RD205")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD205")
    rasterList_road.append(outPath + "RD205")

    print "cycleway..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "cycleway", """"Code" = 206""")
    if int(arcpy.GetCount_management(scratchDB + "cycleway").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD206"):
        arcpy.Delete_management(outPath + "RD206")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "cycleway","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 206, 1)
      rasTemp.save(outPath + "RD206")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD206")
    rasterList_road.append(outPath + "RD206")

    print "path..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "path", """"Code" = 207""")
    if int(arcpy.GetCount_management(scratchDB + "path").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD207"):
        arcpy.Delete_management(outPath + "RD207")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "path","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 207, 1)
      rasTemp.save(outPath + "RD207")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD207")
    rasterList_road.append(outPath + "RD207")

    print "track_grade4..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "track_grade4", """"Code" = 208""")
    if int(arcpy.GetCount_management(scratchDB + "track_grade4").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD208"):
        arcpy.Delete_management(outPath + "RD208")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "track_grade4","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 208, 1)
      rasTemp.save(outPath + "RD208")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD208")
    rasterList_road.append(outPath + "RD208")

    print "track_grade5..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "track_grade5", """"Code" = 209""")
    if int(arcpy.GetCount_management(scratchDB + "track_grade5").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD209"):
        arcpy.Delete_management(outPath + "RD209")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "track_grade5","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 209, 1)
      rasTemp.save(outPath + "RD209")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD209")
    rasterList_road.append(outPath + "RD209")

    print "track..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "track", """"Code" = 210""")
    if int(arcpy.GetCount_management(scratchDB + "track").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD210"):
        arcpy.Delete_management(outPath + "RD210")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "track","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 210, 1)
      rasTemp.save(outPath + "RD210")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD210")
    rasterList_road.append(outPath + "RD210")

    print "service..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "service", """"Code" = 211""")
    if int(arcpy.GetCount_management(scratchDB + "service").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD211"):
        arcpy.Delete_management(outPath + "RD211")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "service","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 211, 1)
      rasTemp.save(outPath + "RD211")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD211")
    rasterList_road.append(outPath + "RD211")

    print "pedestrian..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "pedestrian", """"Code" = 212""")
    if int(arcpy.GetCount_management(scratchDB + "pedestrian").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD212"):
        arcpy.Delete_management(outPath + "RD212")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "pedestrian","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 212, 1)
      rasTemp.save(outPath + "RD212")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD212")
    rasterList_road.append(outPath + "RD212")

    print "track_grade1..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "track_grade1", """"Code" = 213""")
    if int(arcpy.GetCount_management(scratchDB + "track_grade1").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD213"):
        arcpy.Delete_management(outPath + "RD213")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "track_grade1","","1","")
      rasTemp = Con(eucDistTemp < 2.5, 213, 1)
      rasTemp.save(outPath + "RD213")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD213")
    rasterList_road.append(outPath + "RD213")

    print "track_grade2..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "track_grade2", """"Code" = 214""")
    if int(arcpy.GetCount_management(scratchDB + "track_grade2").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD214"):
        arcpy.Delete_management(outPath + "RD214")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "track_grade2","","1","")
      rasTemp = Con(eucDistTemp < 2.5, 214, 1)
      rasTemp.save(outPath + "RD214")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD214")
    rasterList_road.append(outPath + "RD214")

    print "track_grade3..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "track_grade3", """"Code" = 215""")
    if int(arcpy.GetCount_management(scratchDB + "track_grade3").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD215"):
        arcpy.Delete_management(outPath + "RD215")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "track_grade3","","1","")
      rasTemp = Con(eucDistTemp < 2.5, 215, 1)
      rasTemp.save(outPath + "RD215")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD215")
    rasterList_road.append(outPath + "RD215")

    print "tertiary_link..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "tertiary_link", """"Code" = 216""")
    if int(arcpy.GetCount_management(scratchDB + "tertiary_link").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD216"):
        arcpy.Delete_management(outPath + "RD216")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "tertiary_link","","1","")
      rasTemp = Con(eucDistTemp < 2.5, 216, 1)
      rasTemp.save(outPath + "RD216")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD216")
    rasterList_road.append(outPath + "RD216")

    print "tertiary..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "tertiary", """"Code" = 217""")
    if int(arcpy.GetCount_management(scratchDB + "tertiary").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD217"):
        arcpy.Delete_management(outPath + "RD217")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "tertiary","","1","")
      rasTemp = Con(eucDistTemp < 2.5, 217, 1)
      rasTemp.save(outPath + "RD217")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD217")
    rasterList_road.append(outPath + "RD217")

    print "primary_link..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "primary_link", """"Code" = 218""")
    if int(arcpy.GetCount_management(scratchDB + "primary_link").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD218"):
        arcpy.Delete_management(outPath + "RD218")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "primary_link","","1","")
      rasTemp = Con(eucDistTemp < 3, 218, 1)
      rasTemp.save(outPath + "RD218")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD218")
    rasterList_road.append(outPath + "RD218")

    print "primary..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "primary", """"Code" = 219""")
    if int(arcpy.GetCount_management(scratchDB + "primary").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD219"):
        arcpy.Delete_management(outPath + "RD219")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "primary","","1","")
      rasTemp = Con(eucDistTemp < 3, 219, 1)
      rasTemp.save(outPath + "RD219")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD219")
    rasterList_road.append(outPath + "RD219")

    print "secondary_link..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "secondary_link", """"Code" = 220""")
    if int(arcpy.GetCount_management(scratchDB + "secondary_link").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD220"):
        arcpy.Delete_management(outPath + "RD230")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "secondary_link","","1","")
      rasTemp = Con(eucDistTemp < 3, 220, 1)
      rasTemp.save(outPath + "RD220")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD220")
    rasterList_road.append(outPath + "RD220")

    print "living_street..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "living_street", """"Code" = 221""")
    if int(arcpy.GetCount_management(scratchDB + "living_street").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD221"):
        arcpy.Delete_management(outPath + "RD221")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "living_street","","1","")
      rasTemp = Con(eucDistTemp < 3, 221, 1)
      rasTemp.save(outPath + "RD221")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD221")
    rasterList_road.append(outPath + "RD221")

    print "secondary..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "secondary", """"Code" = 230""")
    if int(arcpy.GetCount_management(scratchDB + "secondary").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD230"):
        arcpy.Delete_management(outPath + "RD230")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "secondary","","1","")
      rasTemp = Con(eucDistTemp < 7, 230, 1)
      rasTemp.save(outPath + "RD230")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD230")
    rasterList_road.append(outPath + "RD230")

    print "motorway_link..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "motorway_link", """"Code" = 231""")
    if int(arcpy.GetCount_management(scratchDB + "motorway_link").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD231"):
        arcpy.Delete_management(outPath + "RD231")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "motorway_link","","1","")
      rasTemp = Con(eucDistTemp < 7, 231, 1)
      rasTemp.save(outPath + "RD231")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD231")
    rasterList_road.append(outPath + "RD231")

    print "residential..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "residential", """"Code" = 232""")
    if int(arcpy.GetCount_management(scratchDB + "residential").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD232"):
        arcpy.Delete_management(outPath + "RD232")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "residential","","1","")
      rasTemp = Con(eucDistTemp < 7, 232, 1)
      rasTemp.save(outPath + "RD232")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD232")
    rasterList_road.append(outPath + "RD232")

    print "motorway..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "motorway", """"Code" = 233""")
    if int(arcpy.GetCount_management(scratchDB + "motorway").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD233"):
        arcpy.Delete_management(outPath + "RD233")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "motorway","","1","")
      rasTemp = Con(eucDistTemp < 15, 233, 1)
      rasTemp.save(outPath + "RD233")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD233")
    rasterList_road.append(outPath + "RD233")

    print "trunck_link..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "trunck_link", """"Code" = 234""")
    if int(arcpy.GetCount_management(scratchDB + "trunck_link").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD234"):
        arcpy.Delete_management(outPath + "RD234")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "trunck_link","","1","")
      rasTemp = Con(eucDistTemp < 15, 234, 1)
      rasTemp.save(outPath + "RD234")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD234")
    rasterList_road.append(outPath + "RD234")

    print "trunck..."
    arcpy.Select_analysis(scratchDB + "road_selection",scratchDB +  "trunck", """"Code" = 235""")
    if int(arcpy.GetCount_management(scratchDB + "trunck").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD235"):
        arcpy.Delete_management(outPath + "RD235")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "trunck","","1","")
      rasTemp = Con(eucDistTemp < 15, 235, 1)
      rasTemp.save(outPath + "RD235")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD235")
    rasterList_road.append(outPath + "RD235")


  # 270:280 - railway from rail_line   
  if rail_line == 1:
    print "Processing railways ..."
    arcpy.MakeFeatureLayer_management("rail_line", "rail_layer", "", "", "")
    arcpy.CopyFeatures_management("rail_layer", scratchDB + "rail_selection")

    print "fanicular..."
    arcpy.Select_analysis(scratchDB + "rail_selection",scratchDB +  "fanicular", """"Code" = 270""")
    if int(arcpy.GetCount_management(scratchDB + "fanicular").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD270"):
        arcpy.Delete_management(outPath + "RD270")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "fanicular","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 270, 1)
      rasTemp.save(outPath + "RD270")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD270")
    rasterList_road.append(outPath + "RD270")

    print "miniature_railway"
    arcpy.Select_analysis(scratchDB + "rail_selection",scratchDB +  "miniature_railway", """"Code" = 271""")
    if int(arcpy.GetCount_management(scratchDB + "miniature_railway").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD271"):
        arcpy.Delete_management(outPath + "RD271")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "miniature_railway","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 271, 1)
      rasTemp.save(outPath + "RD271")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD271")
    rasterList_road.append(outPath + "RD271")

    print "monorail..."
    arcpy.Select_analysis(scratchDB + "rail_selection",scratchDB +  "monorail", """"Code" = 272""")
    if int(arcpy.GetCount_management(scratchDB + "monorail").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD272"):
        arcpy.Delete_management(outPath + "RD272")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "monorail","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 272, 1)
      rasTemp.save(outPath + "RD272")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD272")
    rasterList_road.append(outPath + "RD272")

    print "narrow_gauge..."
    arcpy.Select_analysis(scratchDB + "rail_selection",scratchDB +  "narrow_gauge", """"Code" = 273""")
    if int(arcpy.GetCount_management(scratchDB + "narrow_gauge").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD273"):
        arcpy.Delete_management(outPath + "RD273")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "narrow_gauge","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 273, 1)
      rasTemp.save(outPath + "RD273")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD273")
    rasterList_road.append(outPath + "RD273")

    print "subway..."
    arcpy.Select_analysis(scratchDB + "rail_selection",scratchDB +  "subway", """"Code" = 274""")
    if int(arcpy.GetCount_management(scratchDB + "subway").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD274"):
        arcpy.Delete_management(outPath + "RD274")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "subway","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 274, 1)
      rasTemp.save(outPath + "RD274")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD274")
    rasterList_road.append(outPath + "RD274")

    print "tram..."
    arcpy.Select_analysis(scratchDB + "rail_selection",scratchDB +  "tram", """"Code" = 275""")
    if int(arcpy.GetCount_management(scratchDB + "tram").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD275"):
        arcpy.Delete_management(outPath + "RD275")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "tram","","1","")
      rasTemp = Con(eucDistTemp < 1.01, 275, 1)
      rasTemp.save(outPath + "RD275")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD275")
    rasterList_road.append(outPath + "RD275")

    print "rail..."
    arcpy.Select_analysis(scratchDB + "rail_selection",scratchDB +  "rail", """"Code" = 280""")
    if int(arcpy.GetCount_management(scratchDB + "rail").getOutput(0))>0:
      if arcpy.Exists(outPath + "RD280"):
        arcpy.Delete_management(outPath + "RD280")
        print "... deleting existing raster"
      eucDistTemp = EucDistance(scratchDB + "rail","","1","")
      rasTemp = Con(eucDistTemp < 2.5, 280, 1)
      rasTemp.save(outPath + "RD280")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "RD280")
    rasterList_road.append(outPath + "RD280")

## THEME CULTURAL (3)
  print "Processing CULTURAL THEME ..."

  rasterList_cultural = []

  # 321 - point objects
  if tree_point== 1:
    print "Processing point objects ..."
    print "Trees ..."
    arcpy.Select_analysis("tree_point", scratchDB + "tree", """"Code" = 321""")
    if int(arcpy.GetCount_management(scratchDB + "tree").getOutput(0))>0:                           
      if arcpy.Exists(outPath + "CA321"):
        arcpy.Delete_management(outPath + "CA321")
        print "... deleting existing raster"    
      eucDistTemp = EucDistance(scratchDB + "tree", "", "1", "")
      rasTemp = Con(eucDistTemp < 2, 321, 1)
      rasTemp.save(outPath + "CA321")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "CA321")
    rasterList_cultural.append(outPath + "CA321")


## THEME BUILDING (4)
  print "Processing BUILDING THEME ..."
  rasterList_builtup = []

  # 410-460 - built-up areas
  if buildings == 1:
    print "Processing built-up areas ..."
    print "residential ..."
    arcpy.Select_analysis("buildings", scratchDB + "residential", """"Code" = 410""")
    if int(arcpy.GetCount_management(scratchDB + "residential").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU410"):
        arcpy.Delete_management(outPath + "BU410")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "residential", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 410)
      rasTemp.save(outPath + "BU410")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU410")
    rasterList_builtup.append(outPath + "BU410")

    print "parking ..."
    arcpy.Select_analysis("buildings", scratchDB + "parking", """"Code" = 420""")
    if int(arcpy.GetCount_management(scratchDB + "parking").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU420"):
        arcpy.Delete_management(outPath + "BU420")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "parking", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 420)
      rasTemp.save(outPath + "BU420")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU420")
    rasterList_builtup.append(outPath + "BU420")

    print "marina ..."
    arcpy.Select_analysis("buildings", scratchDB + "marina", """"Code" = 421""")
    if int(arcpy.GetCount_management(scratchDB + "marina").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU421"):
        arcpy.Delete_management(outPath + "BU421")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "marina", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 421)
      rasTemp.save(outPath + "BU421")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU421")
    rasterList_builtup.append(outPath + "BU421")
 
    print "fuel ..."
    arcpy.Select_analysis("buildings", scratchDB + "fuel", """"Code" = 422""")
    if int(arcpy.GetCount_management(scratchDB + "fuel").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU422"):
        arcpy.Delete_management(outPath + "BU422")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "fuel", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 422)
      rasTemp.save(outPath + "BU422")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU422")
    rasterList_builtup.append(outPath + "BU422")
 
    print "parking_multistory ..."
    arcpy.Select_analysis("buildings", scratchDB + "parking_multistory", """"Code" = 424""")
    if int(arcpy.GetCount_management(scratchDB + "parking_multistory").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU424"):
        arcpy.Delete_management(outPath + "BU424")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "parking_multistory", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 424)
      rasTemp.save(outPath + "BU424")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU424")
    rasterList_builtup.append(outPath + "BU424")

    print "parking_underground ..."
    arcpy.Select_analysis("buildings", scratchDB + "parking_underground", """"Code" = 425""")
    if int(arcpy.GetCount_management(scratchDB + "parking_underground").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU425"):
        arcpy.Delete_management(outPath + "BU425")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "parking_underground", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 425)
      rasTemp.save(outPath + "BU425")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU425")
    rasterList_builtup.append(outPath + "BU425")

    print "service ..."
    arcpy.Select_analysis("buildings", scratchDB + "service", """"Code" = 426""")
    if int(arcpy.GetCount_management(scratchDB + "service").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU426"):
        arcpy.Delete_management(outPath + "BU426")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "service", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 426)
      rasTemp.save(outPath + "BU426")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU426")
    rasterList_builtup.append(outPath + "BU426")
 
    print "pitch ..."
    arcpy.Select_analysis("buildings", scratchDB + "pitch", """"Code" = 430""")
    if int(arcpy.GetCount_management(scratchDB + "pitch").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU430"):
        arcpy.Delete_management(outPath + "BU430")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "pitch", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 430)
      rasTemp.save(outPath + "BU430")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU430")
    rasterList_builtup.append(outPath + "BU430")
   
    print "playground ..."
    arcpy.Select_analysis("buildings", scratchDB + "playground", """"Code" = 431""")
    if int(arcpy.GetCount_management(scratchDB + "playground").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU431"):
        arcpy.Delete_management(outPath + "BU431")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "playground", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 431)
      rasTemp.save(outPath + "BU431")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU431")
    rasterList_builtup.append(outPath + "BU431")
 
    print "industrial ..."
    arcpy.Select_analysis("buildings", scratchDB + "industrial", """"Code" = 432""")
    if int(arcpy.GetCount_management(scratchDB + "industrial").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU432"):
        arcpy.Delete_management(outPath + "BU432")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "industrial", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 432)
      rasTemp.save(outPath + "BU432")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU432")
    rasterList_builtup.append(outPath + "BU432")
  
    print "cementery ..."
    arcpy.Select_analysis("buildings", scratchDB + "cementery", """"Code" = 433""")
    if int(arcpy.GetCount_management(scratchDB + "cementery").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU433"):
        arcpy.Delete_management(outPath + "BU433")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "cementery", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 433)
      rasTemp.save(outPath + "BU433")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU433")
    rasterList_builtup.append(outPath + "BU433")
  
    print "commercial ..."
    arcpy.Select_analysis("buildings", scratchDB + "commercial", """"Code" = 434""")
    if int(arcpy.GetCount_management(scratchDB + "commercial").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU434"):
        arcpy.Delete_management(outPath + "BU434")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "commercial", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 434)
      rasTemp.save(outPath + "BU434")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU434")
    rasterList_builtup.append(outPath + "BU434")

  
    print "sport_centre ..."
    arcpy.Select_analysis("buildings", scratchDB + "sport_centre", """"Code" = 435""")
    if int(arcpy.GetCount_management(scratchDB + "sport_centre").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU435"):
        arcpy.Delete_management(outPath + "BU435")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "sport_centre", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 435)
      rasTemp.save(outPath + "BU435")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU435")
    rasterList_builtup.append(outPath + "BU435")
  
    print "water_mill ..."
    arcpy.Select_analysis("buildings", scratchDB + "water_mill", """"Code" = 440""")
    if int(arcpy.GetCount_management(scratchDB + "water_mill").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU440"):
        arcpy.Delete_management(outPath + "BU440")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "water_mill", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 440)
      rasTemp.save(outPath + "BU440")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU440")
    rasterList_builtup.append(outPath + "BU440")
  
    print "water_tower ..."
    arcpy.Select_analysis("buildings", scratchDB + "water_tower", """"Code" = 441""")
    if int(arcpy.GetCount_management(scratchDB + "water_tower").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU441"):
        arcpy.Delete_management(outPath + "BU441")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "water_tower", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 441)
      rasTemp.save(outPath + "BU441")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU441")
    rasterList_builtup.append(outPath + "BU441")
  
    print "windmill ..."
    arcpy.Select_analysis("buildings", scratchDB + "windmill", """"Code" = 442""")
    if int(arcpy.GetCount_management(scratchDB + "windmill").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU442"):
        arcpy.Delete_management(outPath + "BU442")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "windmill", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 442)
      rasTemp.save(outPath + "BU442")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU442")
    rasterList_builtup.append(outPath + "BU442")
  
    print "christian ..."
    arcpy.Select_analysis("buildings", scratchDB + "christian", """"Code" = 443""")
    if int(arcpy.GetCount_management(scratchDB + "christian").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU443"):
        arcpy.Delete_management(outPath + "BU443")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "christian", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 443)
      rasTemp.save(outPath + "BU443")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU443")
    rasterList_builtup.append(outPath + "BU443")
  
    print "christian_catholic ..."
    arcpy.Select_analysis("buildings", scratchDB + "christian_catholic", """"Code" = 444""")
    if int(arcpy.GetCount_management(scratchDB + "christian_catholic").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU444"):
        arcpy.Delete_management(outPath + "BU444")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "christian_catholic", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 444)
      rasTemp.save(outPath + "BU444")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU444")
    rasterList_builtup.append(outPath + "BU444")
  
    print "bus_station ..."
    arcpy.Select_analysis("buildings", scratchDB + "bus_station", """"Code" = 445""")
    if int(arcpy.GetCount_management(scratchDB + "bus_station").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU445"):
        arcpy.Delete_management(outPath + "BU445")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "bus_station", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 445)
      rasTemp.save(outPath + "BU445")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU445")
    rasterList_builtup.append(outPath + "BU445")
  
    print "railway_station ..."
    arcpy.Select_analysis("buildings", scratchDB + "railway_station", """"Code" = 446""")
    if int(arcpy.GetCount_management(scratchDB + "railway_station").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU446"):
        arcpy.Delete_management(outPath + "BU446")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "railway_station", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 446)
      rasTemp.save(outPath + "BU446")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU446")
    rasterList_builtup.append(outPath + "BU446")
  
    print "buildings ..."
    arcpy.Select_analysis("buildings", scratchDB + "buildings", """"Code" = 460""")
    if int(arcpy.GetCount_management(scratchDB + "buildings").getOutput(0))>0: 
      if arcpy.Exists(outPath + "BU460"):
        arcpy.Delete_management(outPath + "BU460")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "buildings", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 460)
      rasTemp.save(outPath + "BU460")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "BU460")
    rasterList_builtup.append(outPath + "BU460")
  

## THEME NATURE (5)
  print "Processing NATURE THEME ..."
  rasterList_nature = []

# 511-560 - natural
  if natural == 1:
    print "Processing forest ..."
    print "Mixed ..."
    arcpy.Select_analysis("natural", scratchDB + "mixed_forest", """"HERK2" = 60""")
    if int(arcpy.GetCount_management(scratchDB + "mixed_forest").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA60"):
        arcpy.Delete_management(outPath + "NA60")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "mixed_forest", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 60)
      rasTemp.save(outPath + "NA60")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "NA60")
    rasterList_nature.append(outPath + "NA60")

    print "Deciduous ..."
    arcpy.Select_analysis("natural", scratchDB + "dec_forest", """"HERK2" = 40""")
    if int(arcpy.GetCount_management(scratchDB + "dec_forest").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA40"):
        arcpy.Delete_management(outPath + "NA40")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "dec_forest", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 40)
      rasTemp.save(outPath + "NA40")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "NA40")
    rasterList_nature.append(outPath + "NA40")

    print "Coniferous ..."
    arcpy.Select_analysis("natural", scratchDB + "con_forest", """"HERK2" = 50""")
    if int(arcpy.GetCount_management(scratchDB + "con_forest").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA50"):
        arcpy.Delete_management(outPath + "NA50")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "con_forest", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 50)
      rasTemp.save(outPath + "NA50")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "NA50")
    rasterList_nature.append(outPath + "NA50")

    print "Processing river plants ..."
    arcpy.Select_analysis("natural", scratchDB + "river_plants", """"HERK2" = 98""")
    if int(arcpy.GetCount_management(scratchDB + "river_plants").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA98"):
        arcpy.Delete_management(outPath + "NA98")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "river_plants", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 98)
      rasTemp.save(outPath + "NA98")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "NA98")
    rasterList_nature.append(outPath + "NA98")
                   
    print "Processing meadow ..."
    arcpy.Select_analysis("natural", scratchDB + "meadow", """"Code" = 529""")
    if int(arcpy.GetCount_management(scratchDB + "meadow").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA529"):
        arcpy.Delete_management(outPath + "NA529")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "meadow", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 529)
      rasTemp.save(outPath + "NA529")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "NA529")
    rasterList_nature.append(outPath + "NA529")

    print "Processing urban_park ..."
    arcpy.Select_analysis("natural", scratchDB + "urban_park", """"Code" = 515""")
    if int(arcpy.GetCount_management(scratchDB + "urban_park").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA515"):
        arcpy.Delete_management(outPath + "NA515")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "urban_park", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 515)
      rasTemp.save(outPath + "NA515")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "NA515")
    rasterList_nature.append(outPath + "NA515")

    print "Processing nature reserve ..."
    arcpy.Select_analysis("natural", scratchDB + "nature_reserve", """"Code" = 530""")
    if int(arcpy.GetCount_management(scratchDB + "nature_reserve").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA530"):
        arcpy.Delete_management(outPath + "NA530")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "nature_reserve", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 530)
      rasTemp.save(outPath + "NA530")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "NA530")
    rasterList_nature.append(outPath + "NA530")

    print "Processing grassland ..."
    arcpy.Select_analysis("natural", scratchDB + "grassland", """"Code" = 540""")
    if int(arcpy.GetCount_management(scratchDB + "grassland").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA540"):
        arcpy.Delete_management(outPath + "NA540")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "grassland", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 540)
      rasTemp.save(outPath + "NA540")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "NA540")
    rasterList_nature.append(outPath + "NA540")

    print "Processing heather ..."
    arcpy.Select_analysis("natural", scratchDB + "heather", """"Code" = 550""")
    if int(arcpy.GetCount_management(scratchDB + "heather").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA550"):
        arcpy.Delete_management(outPath + "NA550")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "heather", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 550)
      rasTemp.save(outPath + "NA550")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "NA550")
    rasterList_nature.append(outPath + "NA550")

    print "Processing sand ..."
    arcpy.Select_analysis("natural", scratchDB + "sand", """"Code" = 560""")
    if int(arcpy.GetCount_management(scratchDB + "sand").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA560"):
        arcpy.Delete_management(outPath + "NA560")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "sand", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 560)
      rasTemp.save(outPath + "NA560")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "NA560")
    rasterList_nature.append(outPath + "NA560")

## THEME CULTIVABLE (6)
  print "Processing CULTIVABLE THEME ..."
  rasterList_cultivable = []

# 61 - cultivable areas (PTUT)
  if orchard == 1:

    print "Processing orchards ..."
    arcpy.Select_analysis("orchard", scratchDB + "orchards", "")
    if int(arcpy.GetCount_management(scratchDB + "orchards").getOutput(0))>0: 
      if arcpy.Exists(outPath + "CU610"):
        arcpy.Delete_management(outPath + "CU610")
        print "... deleting existing raster"
      arcpy.PolygonToRaster_conversion(scratchDB + "orchards", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 610)
      rasTemp.save(outPath + "CU610")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print "No such features in the area"
      arcpy.CopyRaster_management(outPath + "land", outPath + "CU610")
    rasterList_cultivable.append(outPath + "CU610")

  if agriculture == 1:
    
    print "Processing fields ..."
    arcpy.MakeFeatureLayer_management("agriculture", "fields1000_lyr")
    arcpy.SelectLayerByAttribute_management("fields1000_lyr", "NEW_SELECTION",)

    cursor = arcpy.UpdateCursor("fields1000_lyr")
    i = 1000
    for row in cursor:
      row.setValue('gridcode', i)
      i = i+1
      cursor.updateRow(row)

    arcpy.SelectLayerByAttribute_management("fields1000_lyr", "CLEAR_SELECTION")
    arcpy.CopyFeatures_management("fields1000_lyr", scratchDB + "fields1000")
    arcpy.FeatureToRaster_conversion(scratchDB + "fields1000", "gridcode", scratchDB + "fields1000_raster", 1)
    outCon = Con(IsNull(Raster(scratchDB + "fields1000_raster")), 1, Raster(scratchDB + "fields1000_raster"))
    outCon.save(outPath + "fields1000")



  rasTemp = ''
  rasTemp1 = ''
  print " "
#===== End Chunk: Conversion =====#

#===== Chunk: Themes =====#
 #Combine rasters to thematic maps 

  if water == 1:   #Assembles a water theme
    print "Processing water layers ..."
    if arcpy.Exists(outPath + "T1_water"):
      arcpy.Delete_management(outPath + "T1_water")
      print "... deleting existing raster"
    print rasterList_water
    rasTemp = CellStatistics(rasterList_water, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T1_water")

  if road == 1:   #Assembles a road theme
    print "Processing road layers ..."
    if arcpy.Exists(outPath + "T2_road"):
      arcpy.Delete_management(outPath + "T2_road")
      print "... deleting existing raster"
    print rasterList_road
    rasTemp = CellStatistics(rasterList_road, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T2_road")

  if cultural == 1:   #Assembles a cultural theme
    print "Processing cultural layers ..."
    if arcpy.Exists(outPath + "T3_cultural"):
      arcpy.Delete_management(outPath + "T3_cultural")
      print "... deleting existing raster"
    print rasterList_cultural
    rasTemp = CellStatistics(rasterList_cultural, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T3_cultural")

  if builtup == 1:   #Assembles a builtup theme
    print "Processing builtup layers ..."
    if arcpy.Exists(outPath + "T4_builtup"):
      arcpy.Delete_management(outPath + "T4_builtup")
      print "... deleting existing raster"
    print rasterList_builtup
    rasTemp = CellStatistics(rasterList_builtup, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T4_builtup")

  if nature == 1:   #Assembles a nature theme
    print "Processing nature layers ..."
    if arcpy.Exists(outPath + "T5_nature"):
      arcpy.Delete_management(outPath + "T5_nature")
      print "... deleting existing raster"
    print rasterList_nature
    rasTemp = CellStatistics(rasterList_nature, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T5_nature")

  if cultivable == 1:   #Assembles a cultivable theme
    print "Processing cultivable layers ..."
    if arcpy.Exists(outPath + "T6_cultivable"):
      arcpy.Delete_management(outPath + "T6_cultivable")
      print "... deleting existing raster"
    print rasterList_cultivable
    rasTemp = CellStatistics(rasterList_cultivable, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T6_cultivable")

  # Preparing soil info for fields
    arcpy.PolygonToRaster_conversion("soil_map", "Textuur", scratchDB + "soil_raster", "CELL_CENTER", "NONE", "1")
    outZSaT = ZonalStatisticsAsTable(scratchDB + "fields1000", "gridcode", scratchDB + "soil_raster", "C:\Users\dclaeysb\documenten\landscape generation Almass\GIS\soil map geodatabase.gdb", "DATA", "MAJORITY")


#===== End Chunk: Themes =====#fina

#===== Chunk: Stack =====#
# Assemble the final map
# First delete any existing layers
  if finalmap == 1:   
    print "Processing mosaic for all themes ..."
    if arcpy.Exists(outPath + "MapReclassified"):
      arcpy.Delete_management(outPath + "MapReclassified")
      print "... deleting existing raster"
    if arcpy.Exists(outPath + "MapRaw"):
      arcpy.Delete_management(outPath + "MapRaw")
      print "... deleting existing raster"
    if arcpy.Exists(outPath + "MapFinal"):
      arcpy.Delete_management(outPath + "MapFinal")
      print "... deleting existing raster"

  print " "

# Stack the thematic maps 
# Here the hierarchy of individual themes is determined
  T1wa = Raster(outPath + "T1_water")
  T2ro = Raster(outPath + "T2_road")
  T3cu = Raster(outPath + "T3_cultural")
  T4bu = Raster(outPath + "T4_builtup")
  T5na = Raster(outPath + "T5_nature")
  T6cul = Raster(outPath + "T6_cultivable")
  field = Raster(outPath + "fields1000")   # fields
  


  step1 = Con(field > 999, field, 1)                    # fields first
  print "fields added to map ..."
  step2 = Con(step1 == 1, T6cul, step1)                   # other cultivable areas on NOT (fields)
  print "other cultivable areas added to map ..."                 
  step3 = Con(step2 == 1, T5na, step2)                  # natural areas on NOT (fields, cultivable)
  print "natural areas added to map ..."
  step4 = Con(step3 == 1, T4bu, step3)                  # built up areas on NOT (fields, cultivable, natural areas)
  print "built up areas added to map ..."
  step5 = Con(T3cu == 1, step4, T3cu)                  # cultural features on top
  print "cultural landscape features added to map ..."
  step6 = Con(T1wa == 1, step5, T1wa)                   # freshwater on top
  print "fresh water added to map ..."
  map01 = Con(T2ro == 1, step6, T2ro)                   # roads on top
  print "roads added to map ..."

  map01.save (scratchDB + "MapRaw01")


  # Adding road verges to road pixels adjacent to fields or backround pixels         
  MapRaw_focal_max = FocalStatistics(scratchDB + "MapRaw01", NbrRectangle(3, 3, "CELL"), "MAXIMUM","")
  #MapRaw_focal_max.save (scratchDB + "MapRaw_focal_max") 
  MapRaw_focal_min = FocalStatistics(scratchDB + "MapRaw01", NbrRectangle(3, 3, "CELL"), "MINIMUM","")
  #MapRaw_focal_min.save (scratchDB + "MapRaw_focal_min")
  outCon1 = Con((MapRaw_focal_max >= 1000) | (MapRaw_focal_min == 1),1,0)
  outCon2 = Con((map01 >= 200) & (map01 <= 235),1,0)
  outCon = Con((outCon1 == 1) & (outCon2 == 1), 1, 0)
  map02 = Con(outCon == 1, 207, map01)
  map02.save (outPath + "MapRaw")

  nowTime = time.strftime('%X %x')
  print "Raw landscape map created ..." + nowTime
  print "  "
#===== End Chunk: Stack =====#

except:
  if arcpy.Exists(outPath + "tmpRaster"):
      arcpy.Delete_management(outPath + "tmpRaster")
  tb = sys.exc_info()[2]
  tbinfo = traceback.format_tb(tb)[0]
  pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " +        str(sys.exc_type) + ": " + str(sys.exc_value) + "\n"
  msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

  arcpy.AddError(msgs)
  arcpy.AddError(pymsg)

  print msgs
  print pymsg

  arcpy.AddMessage(arcpy.GetMessages(1))
  print arcpy.GetMessages(1)

