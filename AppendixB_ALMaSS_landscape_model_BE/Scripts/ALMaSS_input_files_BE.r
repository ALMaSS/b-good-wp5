# Make a polyref and farmref file to go with the landscape
# Author: Lars Dalby, modified by Elzbieta Ziolkowska on 08.2017, last update 09.2018
library(pracma)
library(devtools) # Needed to install from github
install_github('LDalby/ralmass')
library(ralmass)
library(data.table) # We use the data.table package to import the data files (some of them are big) as the fread function is very fast. We refer the user to the data.table documentation for detail on the syntax.

# Setting the landscape and working directory
LandscapeName = 'Merendree'
PathToFile = paste('C:/Users/dclaeysb/documenten/landscape generation Almass/OutputMerendree/', sep='')

# Import the attribute table exported from ArcGIS
AttrFileName = 'ATTR_Merendree.txt'
attr = fread(paste(PathToFile, AttrFileName, sep = ''))
# Use CleanAttrTable in the ralmass package to clean the file:
# see ?CleanAttrTable for documentation
cleanattr = CleanAttrTable(AttrTable = attr, Soiltype = TRUE)
dim(cleanattr)
setkey(cleanattr, 'PolyType') # See ?setkey for documentation
# Here we seperate the fields and permanent crops form the rest of the polygons
# They are treated slightly different from the rest.
targetfarms = cleanattr[PolyType >= 1000] # The fields
targetfarms[,Soiltype:=NULL] # Remove dummy variable, since real soil type data for the fields is available, will be added further down
orchards = cleanattr[PolyType == 56] # The orchards
tree_fruit_nursery = cleanattr[PolyType == 214] # Tree and fruit plantations

cleanattr = cleanattr[PolyType < 1000 & PolyType!= 56 & PolyType!=214] # Everything else
dim(cleanattr)
str(targetfarms)

#Next we read in the farm information. In this example the data is stored in a text file where each
#field is a row in the data set. Each field has a unique id (no_owner) for the farm to which it belongs.
farm = fread(paste('C:/Users/dclaeysb/documenten/landscape generation Almass/OutputMerendree/fields_and_farms.csv', sep=''))

farminfo = farm[, c('gridcode', 'IDNR', 'FARM_CL_NO'), with = FALSE]
#create new column with re-numbered owner codes from 0 to ...
IDNR=sort(unique(farminfo$IDNR))
farm_code=seq(0,(length(IDNR)-1), 1)
reorder_table=cbind(IDNR, farm_code)
temp1 = merge(farminfo, reorder_table, by='IDNR')
farminfo = temp1
names(farminfo)[2] = 'PolyType'
setkey(farminfo, 'PolyType')

# We have soil information for the fields, so we load that
soil = fread(paste('C:/Users/dclaeysb/documenten/landscape generation Almass/OutputMerendree/soilmapMerendree.txt', sep=''))
setnames(soil, old = 'MAJORITY', new = 'Soiltype')
setnames(soil, old = 'GRIDCODE', new = 'PolyType')
soil_farm = soil[PolyType >= 1000]
soil_farm[,Rowid_:=NULL]
soil_farm[,Join_Count:=NULL]
soil_farm[,AREA:=NULL]
#soil_farm=soil_farm[!duplicated(soil_farm[,3]),]
setkey(soil_farm, 'PolyType')

temp = merge(x = targetfarms, y = farminfo, by='PolyType', all.x=TRUE)
temp2 = merge(x = temp, y = soil_farm, by='PolyType', all.x=TRUE)
temp2[,PolyType:=rep(20, length(temp$PolyType))]
temp2[,FarmRef:=farm_code]
temp2[,farm_code:=NULL]
temp2[,IDNR:=NULL]
temp2[,FARM_CL_NO:=NULL]

temp2$FarmRef[which(is.na(temp2$FarmRef))]<-0

freq_soiltype <- table(as.vector(temp2$Soiltype))
temp2$Soiltype[is.na(temp2$Soiltype)] <- as.numeric(names(freq_soiltype)[freq_soiltype == max(freq_soiltype)])


# Handling orchards and plantations
orchards[,FarmRef:=rep(max(temp2$FarmRef[!is.na(temp2$FarmRef)])+1, length(orchards$PolyType))]
tree_fruit_nursery[,FarmRef:=rep(max(temp2$FarmRef[!is.na(temp2$FarmRef)])+2, length(tree_fruit_nursery$PolyType))]

# This is essentially putting the fields and everything else back together.
result = rbind(cleanattr, orchards, tree_fruit_nursery, temp2) 
# Check that the dimensions match the original input:
dim(attr)
dim(result)
# if something wrong better to check if there is no duplicates
result=result[!duplicated(result),]
setkey(result, 'PolyRefNum')

# Writing PolyRef file
PolyRefFileName = paste('PolyRef_FM100.txt', sep = '')
WritePolyref(Table = result, PathToFile = paste(PathToFile, PolyRefFileName, sep = '')) # The function WritePolyref ensures that
# the resulting complies with the format required by ALMaSS. see ?WritePolyref for documentation.

# Preparing FarmRef file
farmref=farminfo[,c('IDNR', 'FARM_CL_NO')]
farmref=farmref[!duplicated(farmref),]
farmref=merge(reorder_table, farmref, by="IDNR")
farmref=as.data.table(farmref)
farmref[,IDNR:=NULL]
#Adding orchards and plantations
permanent_crops=data.frame(c(max(temp2$FarmRef[!is.na(temp2$FarmRef)])+1, max(temp2$FarmRef[!is.na(temp2$FarmRef)])+2), c(36, 36))
names(permanent_crops)=c("farm_code", "FARM_CL_NO")
farmref=rbindlist(list(farmref, permanent_crops), use.names=TRUE)
#Giving farm with no information on type, the most common type in a given landscape
farmref$FARM_CL_NO[which(farmref$FARM_CL_NO==0)]<-Mode(farmref$FARM_CL_NO)
table(farmref$FARM_CL_NO)

# Writing FarmRef file
FileName = 'FarmRef.txt'
WriteAlmassInput(farmref, paste(PathToFile, FileName, sep=''), headers=FALSE)

