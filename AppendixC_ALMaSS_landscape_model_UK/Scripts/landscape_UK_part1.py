#===================================================================================================#
#Name: Landscape generator for the UK, part1
# Purpose: The script coverts feature layers to rasters and assembles a raw surface covering land-use map.
# Authors: original script for Denmark by Flemming Skov & Lars Dalby - Oct-Dec 2014; modified for Poland by Elzbieta Ziolkowska - Feb 2017, modified for Belgium by David Claeys Bouuaert - december 2019
# Last large update: december 2019
#This script is modified for UK by Adam McVeigh - October 2020 - January 2021


#====Chunk: Setup ====#
import arcpy, traceback, sys, time, gc, os, shutil, csv, fnmatch
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
arcpy.env.parallelProcessingFactor = "100%"
arcpy.env.overwriteOutput = True
sr = arcpy.SpatialReference(27700)
#nowTime = time.strftime('%X %x') # see the more "dynamic" timestamp method that is used 2 lines lower, and elsewhere in the script
gc.enable
print ("Model landscape generator part 1 started") + time.strftime("%H:%M:%S", time.localtime())
print ("... system modules checked")

# Data - paths to data, output gdb, scratch folder and simulation landscape mask

template = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes/test_areas/test_area/" # Path to the template directory
dst = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes/test_areas/"  # Output destination main folder
#nationalDB = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes/data/UK_data.gdb/" # Folder with national data
nationalDB = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes/data/UK_data/" # Folder with national data

landscape = "StudyArea1" # Landscape name to process
landscape_mask = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes/data/" + landscape + ".shp"


####  THIS PART MAKES THE FOLDERS NECESSARY - REMEMBER TO UN-COMMENT FIRST TIME
#dstpath = os.path.join(dst,landscape)
#shutil.copytree(template, dstpath)
#gdbpath = os.path.join(dstpath, "test_area.gdb")
#newgdbpath = os.path.join(dstpath, landscape)
#os.rename(gdbpath, newgdbpath + ".gdb")
#gdbpath2 = os.path.join(dstpath, "outputs","test_area.gdb")
#newgdbpath2 = os.path.join(dstpath, "outputs", landscape)
#os.rename(gdbpath2, newgdbpath2 + ".gdb")

# Data - paths to data, output gdb, scratch folder and simulation landscape mask
outPath = os.path.join(dst, landscape, "outputs", landscape + ".gdb/")
gisDB = os.path.join(dst, landscape, landscape + ".gdb/")
scratchDB = os.path.join(dst, landscape, "outputs/scratch.gdb/")  # scratch folder for shp tempfiles
scratch = os.path.join(dst, landscape, "outputs/scratch/")  # scratch folder for raster tempfiles

#localSettings = project folder with mask
out_feature = os.path.join(dst, landscape, "outputs/project.gdb/", landscape + "_mask")
arcpy.CopyFeatures_management(landscape_mask, out_feature)
localSettings = out_feature

mask = os.path.join(dst, landscape,"outputs/project.gdb/mask")
arcpy.PolygonToRaster_conversion(out_feature, "OBJECTID", mask, "CELL_CENTER", "NONE", "1") # mask in a raster format

#Model Settin
arcpy.env.overwriteOutput = True
arcpy.env.workspace = gisDB
arcpy.env.scratchWorkspace = scratch
arcpy.env.extent = localSettings
arcpy.env.mask = localSettings
arcpy.env.cellSize = localSettings
print ("... model settings read")

#Model execution - controls which processes to run:
default = 1 # 1 -> run process; 0 -> do not run process

#Mosaic
road = default #create road theme
builtup = default #create built up theme
nature = default       #create nature theme
water = default   #create fresh water theme
cultural = default      #create cultural feature theme
cultivable = default #create cultivable land theme
finalmap = default      #assemble final map

rasterList_water = []
rasterList_buffer_water_zone = []
rasterList_transport = []
rasterList_road_verges = []
rasterList_builtup = []
rasterList_cultural = []
rasterList_nature = []
rasterList_cultivable = []

landsea = default

print (" ")
#===== End chunk: setup ===== #


################################################################################

try:

##==== Chunk: Clipping ====#
 #from national data to a given study area

  print ("Clipping national data to the study area extent")

  for root, dirs, datasets in arcpy.da.Walk(nationalDB): #the national GB is a folder with shp files so ds object has an ".shp"
    print (datasets)
    for ds in datasets:
      print ("Clipping ") + ds + " ..."
      arcpy.Clip_analysis(os.path.join(root, ds), localSettings, gisDB + ds[:-4], "")

##===== End: Clipping =====#

##===== Chunk: Conversion ===== #
## from feature layers to raster

## Check which layers exists in gisDB
  layersToProcess = ["RoadLink"]
  for layer in layersToProcess:
    if arcpy.Exists(gisDB + layer):
      if int(arcpy.GetCount_management(gisDB + layer).getOutput(0))>0:
        exec("%s = %d" % (layer,1))
      else:
        exec("%s = %d" % (layer,0))
    else:
      exec("%s = %d" % (layer,0))

## Check which layers exists in folder
  if int(arcpy.GetCount_management(gisDB + "WATER_AREA").getOutput(0))>0:
    WATER_AREA = 1
  else:
    WATER_AREA = 0
    print ("WATER_AREA layer empty")
  if int(arcpy.GetCount_management(gisDB + "WATER_LINE_B").getOutput(0))>0:
    WATER_LINE_B = 1
  else:
    WATER_LINE_B = 0
    print ("WATER_LINE_B layer empty")
  if int(arcpy.GetCount_management(gisDB + "WATER_NETWORK").getOutput(0))>0:
    WATER_NETWORK = 1
  else:
    WATER_NETWORK = 0
    print ("WATER_NETWORK layer empty")
  if int(arcpy.GetCount_management(gisDB + "ROAD").getOutput(0))>0:
    ROAD = 1
  else:
    ROAD = 0
    print ("ROAD layer empty")
  if int(arcpy.GetCount_management(gisDB + "RAIL").getOutput(0))>0:
    RAIL = 1
  else:
    RAIL = 0
    print ("RAIL layer empty")
  if int(arcpy.GetCount_management(gisDB + "CULTURAL").getOutput(0))>0:
    CULTURAL = 1
  else:
    CULTURAL = 0
    print ("CULTURAL layer empty")    
  if int(arcpy.GetCount_management(gisDB + "URBAN_AREA").getOutput(0))>0:
    URBAN_AREA = 1
  else:
    URBAN_AREA = 0
    print ("URBAN_AREA layer empty")
  if int(arcpy.GetCount_management(gisDB + "GREENSPACEBUILDINGS").getOutput(0))>0:
    GREENSPACEBUILDINGS = 1
  else:
    GREENSPACEBUILDINGS = 0
    print ("GREENSPACEBUILDINGS layer empty")
  if int(arcpy.GetCount_management(gisDB + "GLASSHOUSE").getOutput(0))>0:
    GLASSHOUSE = 1
  else:
    GLASSHOUSE = 0
    print ("GLASSHOUSE layer empty")
  if int(arcpy.GetCount_management(gisDB + "BUILDINGS").getOutput(0))>0:
    BUILDINGS = 1
  else:
    BUILDINGS = 0
    print ("BUILDINGS layer empty")
  if int(arcpy.GetCount_management(gisDB + "NATURE_AREA").getOutput(0))>0:
    NATURE_AREA = 1
  else:
    NATURE_AREA = 0
    print ("NATURE_AREA layer empty")
  if int(arcpy.GetCount_management(gisDB + "NATURE_LINE").getOutput(0))>0:
    NATURE_LINE = 1
  else:
    NATURE_LINE = 0
    print ("NATURE_LINE layer empty")
  if int(arcpy.GetCount_management(gisDB + "LPIS_AND_CROPS_270122").getOutput(0))>0:
    LPIS_AND_CROPS_270122 = 1
  else:
    LPIS_AND_CROPS_270122 = 0
    print ("LPIS_AND_CROPS_270122 layer empty")
  if int(arcpy.GetCount_management(gisDB + "AGRICULTURECROPS").getOutput(0))>0:
    AGRICULTURECROPS = 1
  else:
    AGRICULTURECROPS = 0
    print ("AGRICULTURECROPS layer empty")
  if int(arcpy.GetCount_management(gisDB + "AGRICULTUREARABLE").getOutput(0))>0:
    AGRICULTUREARABLE = 1
  else:
    AGRICULTUREARABLE = 0
    print ("AGRICULTUREARABLE layer empty")

  landsea = default
      
  # land
  if landsea == 1:
    print ("Processing base map (land/sea) ...")
    if arcpy.Exists(outPath + "landsea"):
      arcpy.Delete_management(outPath + "landsea")
      print ("... deleting existing raster")
    rasTemp = Con(Raster(mask) >= 0, 1, 0)
    rasTemp.save(outPath + "landsea")

##    ## THEME WATER (1)
  print ("Processing WATER THEME...")
  print ("Processing WATER_LINE_B")

  rasterList_water = []
    
## 113:115 -  WATER_LINE_B   
  if WATER_LINE_B == 1:
    print ("Processing canal ...")
    if int(arcpy.GetCount_management("WATER_LINE_B").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("WATER_LINE_B", "selectlyr")
    if arcpy.Exists(outPath + "WA102"):
      arcpy.Delete_management(outPath + "WA102")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "canal", """"code" = 102""")
    if int(arcpy.GetCount_management("canal").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("canal","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 2, 102, 1)
      rasTemp.save(outPath + "WA102")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "WA102", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_water.append(outPath + "WA102")

    print ("Processing inlandRiver ...")
    if int(arcpy.GetCount_management("WATER_LINE_B").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("WATER_LINE_B", "selectlyr")
    if arcpy.Exists(outPath + "WA104"):
      arcpy.Delete_management(outPath + "WA104")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "inlandRiver", """"code" = 104""")
    if int(arcpy.GetCount_management("inlandRiver").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("inlandRiver","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 2, 104, 1)
      rasTemp.save(outPath + "WA104")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "WA104", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_water.append(outPath + "WA104")

    print ("Processing tidalRiver ...")
    if int(arcpy.GetCount_management("WATER_LINE_B").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("WATER_LINE_B", "selectlyr")
    if arcpy.Exists(outPath + "WA105"):
      arcpy.Delete_management(outPath + "WA105")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "tidalRiver", """"code" = 105""")
    if int(arcpy.GetCount_management("tidalRiver").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("tidalRiver","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 5, 105, 1)
      rasTemp.save(outPath + "WA105")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "WA105", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_water.append(outPath + "WA105")

    print ("Processing lake ...")
    if int(arcpy.GetCount_management("WATER_LINE_B").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("WATER_LINE_B", "selectlyr")
    if arcpy.Exists(outPath + "WA106"):
      arcpy.Delete_management(outPath + "WA106")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "lake", """"code" = 106""")
    if int(arcpy.GetCount_management("lake").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("lake","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 10, 106, 1)
      rasTemp.save(outPath + "WA106")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "WA106", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_water.append(outPath + "WA106")

## WATER_NETWORK   
  if WATER_NETWORK == 1:
    print ("WATER_NETWORK ...")
    if int(arcpy.GetCount_management("WATER_NETWORK").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("WATER_NETWORK", "selectlyr")
    if arcpy.Exists(outPath + "WA150"):
      arcpy.Delete_management(outPath + "WA150")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "150", """"code" = 150""")
    if int(arcpy.GetCount_management("canal").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("canal","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 2, 150, 1)
      rasTemp.save(outPath + "WA150")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "WA150", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_water.append(outPath + "WA150")
                              

## 110+116-125 - WATER_AREA
  if WATER_AREA == 1:
    print ("Processing WATER_AREA layer ...")

    print ("Processing Freshwater ...")
    arcpy.Select_analysis("WATER_AREA", scratchDB + "Freshwater", """"code" = 101""")
    if int(arcpy.GetCount_management(scratchDB + "Freshwater").getOutput(0))>0:
      if arcpy.Exists(outPath + "WA101"):
        arcpy.Delete_management(outPath + "WA101")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Freshwater", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"),1, 101)
      rasTemp.save(outPath + "WA101")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "WA101", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_water.append(outPath + "WA101")

    print ("Processing Saltwater ...")
    arcpy.Select_analysis("WATER_AREA", scratchDB + "Saltwater", """"code" = 107""")
    if int(arcpy.GetCount_management(scratchDB + "Saltwater").getOutput(0))>0:
      if arcpy.Exists(outPath + "WA107"):
        arcpy.Delete_management(outPath + "WA107")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Saltwater", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"),1, 107)
      rasTemp.save(outPath + "WA107")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "WA107", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_water.append(outPath + "WA107")

# # Creating 1-m buffer(unsprayed) zones around all water
    print ("rasterList_water")
    maxRaster = CellStatistics(rasterList_water, "MAXIMUM", "DATA")
    outSetNull = SetNull(maxRaster, 1, "VALUE = 1")
    eucDistTemp = EucDistance(outSetNull,"","1","")
    rasTemp = Con((eucDistTemp < 1.50) & (eucDistTemp > 0), 100, 1)
    rasTemp.save(outPath + "WA100")
    rasterList_water.append(outPath + "WA100")

##   ## THEME ROADS (2)
  print ("Processing ROAD THEME ...")

  rasterList_transport = []

## 201-235 - roads from ROAD
  if ROAD == 1:
    print ("Processing Secondary_Access_Road ...")
    if int(arcpy.GetCount_management("ROAD").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("ROAD", "selectlyr")
    if arcpy.Exists(outPath + "RD201"):
      arcpy.Delete_management(outPath + "RD201")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "Secondary_Access_Road", """"code" = 201""")
    if int(arcpy.GetCount_management("Secondary_Access_Road").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("Secondary_Access_Road","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 1.5, 201, 1)
      rasTemp.save(outPath + "RD201")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RD201", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RD201")

    print ("Processing Restricted_Local_Access_Road ...")
    if int(arcpy.GetCount_management("ROAD").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("ROAD", "selectlyr")
    if arcpy.Exists(outPath + "RD202"):
      arcpy.Delete_management(outPath + "RD202")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "Restricted_Local_Access_Road", """"code" = 202""")
    if int(arcpy.GetCount_management("Restricted_Local_Access_Road").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("Restricted_Local_Access_Road","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 1.6, 202, 1)
      rasTemp.save(outPath + "RD202")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RD202", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RD202")

    print ("Processing Local_Access_Road ...")
    if int(arcpy.GetCount_management("ROAD").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("ROAD", "selectlyr")
    if arcpy.Exists(outPath + "RD203"):
      arcpy.Delete_management(outPath + "RD203")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "Local_Access_Road", """"code" = 203""")
    if int(arcpy.GetCount_management("Local_Access_Road").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("Local_Access_Road","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 1.6, 203, 1)
      rasTemp.save(outPath + "RD203")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RD203", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RD203")

    print ("Processing Local_Road ...")
    if int(arcpy.GetCount_management("ROAD").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("ROAD", "selectlyr")
    if arcpy.Exists(outPath + "RD204"):
      arcpy.Delete_management(outPath + "RD204")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "Local_Road", """"code" = 204""")
    if int(arcpy.GetCount_management("Local_Road").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("Local_Road","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 2.75, 204, 1)
      rasTemp.save(outPath + "RD204")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RD204", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RD204")

    print ("Processing Minor_Road ...")
    if int(arcpy.GetCount_management("ROAD").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("ROAD", "selectlyr")
    if arcpy.Exists(outPath + "RD205"):
      arcpy.Delete_management(outPath + "RD205")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "Minor_Road", """"code" = 205""")
    if int(arcpy.GetCount_management("Minor_Road").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("Minor_Road","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 3.1, 205, 1)
      rasTemp.save(outPath + "RD205")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RD205", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RD205")

    print ("Processing B_Road ...")
    if int(arcpy.GetCount_management("ROAD").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("ROAD", "selectlyr")
    if arcpy.Exists(outPath + "RD206"):
      arcpy.Delete_management(outPath + "RD206")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "B_Road", """"code" = 206""")
    if int(arcpy.GetCount_management("B_Road").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("B_Road","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 3.75, 206, 1)
      rasTemp.save(outPath + "RD206")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RD206", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RD206")

    print ("Processing A_Road ...")
    if int(arcpy.GetCount_management("ROAD").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("ROAD", "selectlyr")
    if arcpy.Exists(outPath + "RD207"):
      arcpy.Delete_management(outPath + "RD207")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "A_Road", """"code" = 207""")
    if int(arcpy.GetCount_management("A_Road").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("A_Road","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 5.6, 207, 1)
      rasTemp.save(outPath + "RD207")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RD207", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RD207")

    print ("Processing Motorway ...")
    if int(arcpy.GetCount_management("ROAD").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("ROAD", "selectlyr")
    if arcpy.Exists(outPath + "RD208"):
      arcpy.Delete_management(outPath + "RD208")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "Motorway", """"code" = 208""")
    if int(arcpy.GetCount_management("Motorway").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("Motorway","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 14.25, 208, 1)
      rasTemp.save(outPath + "RD208")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RD208", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RD208")

## Creating 1-m road verges around all roads and railways
    print ("rasterList_transport")
    maxRaster = CellStatistics(rasterList_transport, "MAXIMUM", "DATA")
    outSetNull = SetNull(maxRaster, 1, "VALUE = 1")
    eucDistTemp = EucDistance(outSetNull,"","1","")
    rasTemp = Con((eucDistTemp < 1.50) & (eucDistTemp > 0), 210, 1)
    rasTemp.save(outPath + "RD210")
    rasterList_transport.append(outPath + "RD210")
    
##  # 270:280 - RAIL   
  if RAIL == 1:
    print ("Processing Railwyas ...")
    
    print ("Processing Narrow_Gauge ...")
    if int(arcpy.GetCount_management("RAIL").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("RAIL", "selectlyr")
    if arcpy.Exists(outPath + "RA210"):
      arcpy.Delete_management(outPath + "RA210")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "Narrow_Gauge", """"code" = 210""")
    if int(arcpy.GetCount_management("Narrow_Gauge").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("Narrow_Gauge","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 0.49, 210, 1)
      rasTemp.save(outPath + "RA210")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RA210", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RA210")

    print ("Processing Single_Track ...")
    if int(arcpy.GetCount_management("RAIL").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("RAIL", "selectlyr")
    if arcpy.Exists(outPath + "RA211"):
      arcpy.Delete_management(outPath + "RA211")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "Single_Track", """"code" = 211""")
    if int(arcpy.GetCount_management("Single_Track").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("Single_Track","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 0.72, 211, 1)
      rasTemp.save(outPath + "RA211")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RA211", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RA211")

    print ("Processing Multi_Track ...")
    if int(arcpy.GetCount_management("RAIL").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("RAIL", "selectlyr")
    if arcpy.Exists(outPath + "RA212"):
      arcpy.Delete_management(outPath + "RA212")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "Multi_Track", """"code" = 212""")
    if int(arcpy.GetCount_management("Multi_Track").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("Multi_Track","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 5.3, 212, 1)
      rasTemp.save(outPath + "RA212")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "RA212", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_transport.append(outPath + "RA212")
    
##  ## THEME CULTURAL (3)
  print ("Processing CULTURAL THEME ...")

  rasterList_cultural = []

  if CULTURAL == 1:
    print ("Processing CULTURAL ...")
    print ("Processing Allotments_Or_Community_Spaces ...")
    arcpy.Select_analysis("CULTURAL", scratchDB + "Allotments_Or_Community_Spaces", """"code" = 301""")
    if int(arcpy.GetCount_management(scratchDB + "Allotments_Or_Community_Spaces").getOutput(0))>0:
      if arcpy.Exists(outPath + "C301"):
        arcpy.Delete_management(outPath + "C301")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Allotments_Or_Community_Spaces", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"),1, 301)
      rasTemp.save(outPath + "C301")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "C301", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_cultural.append(outPath + "C301")

    print ("Processing Cemetery ...")
    arcpy.Select_analysis("CULTURAL", scratchDB + "Cemetery", """"code" = 302""")
    if int(arcpy.GetCount_management(scratchDB + "Cemetery").getOutput(0))>0:
      if arcpy.Exists(outPath + "C302"):
        arcpy.Delete_management(outPath + "C302")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Cemetery", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"),1, 302)
      rasTemp.save(outPath + "C302")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "C302", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_cultural.append(outPath + "C302")

    print ("Processing Public_Park_Or_Garden ...")
    arcpy.Select_analysis("CULTURAL", scratchDB + "Public_Park_Or_Garden", """"code" = 303""")
    if int(arcpy.GetCount_management(scratchDB + "Public_Park_Or_Garden").getOutput(0))>0:
      if arcpy.Exists(outPath + "C303"):
        arcpy.Delete_management(outPath + "C303")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Public_Park_Or_Garden", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"),1, 303)
      rasTemp.save(outPath + "C303")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "C303", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_cultural.append(outPath + "C303")

## THEME BUILDING (4)
  print ("Processing BUILDING THEME ...")
  rasterList_builtup = []

##URBAN_AREA layer
  print ("Processing URBAN_AREA ...")
  if URBAN_AREA == 1:
  
   print ("Processing Suburban and Urban ...")
   arcpy.Select_analysis("URBAN_AREA", scratchDB + "Suburban", """"code" = 401""")
   if int(arcpy.GetCount_management(scratchDB + "Suburban").getOutput(0))>0:
      if arcpy.Exists(outPath + "B401"):
        arcpy.Delete_management(outPath + "B401")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Suburban", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"),1, 401)
      rasTemp.save(outPath + "B401")
      arcpy.Delete_management(outPath + "tmpRaster")
  else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "B401", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
  rasterList_builtup.append(outPath + "B401")
                    
  print ("Processing Urban ...")
  arcpy.Select_analysis("URBAN_AREA", scratchDB + "Urban", """"Code" = 402""")
  if int(arcpy.GetCount_management(scratchDB + "Urban").getOutput(0))>0: 
      if arcpy.Exists(outPath + "B402"):
        arcpy.Delete_management(outPath + "B402")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Urban", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 402)
      rasTemp.save(outPath + "B402")
      arcpy.Delete_management(outPath + "tmpRaster")
  else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "B402", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
  rasterList_builtup.append(outPath + "B402")

##in GREENSPACEBUILDINGS layer
  print ("Processing GREENSPACEBUILDINGS ...")
  print ("Processing Playing_Field ...")
  arcpy.Select_analysis("GREENSPACEBUILDINGS", scratchDB + "Playing_Field", """"Code" = 403""")
  if int(arcpy.GetCount_management(scratchDB + "Playing_Field").getOutput(0))>0: 
      if arcpy.Exists(outPath + "B403"):
        arcpy.Delete_management(outPath + "B403")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Playing_Field", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 403)
      rasTemp.save(outPath + "B403")
      arcpy.Delete_management(outPath + "tmpRaster")
  else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "B404", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
  rasterList_builtup.append(outPath + "B404")

  print ("Processing Other_Sports_Facility ...")
  arcpy.Select_analysis("GREENSPACEBUILDINGS", scratchDB + "Other_Sports_Facility", """"Code" = 405""")
  if int(arcpy.GetCount_management(scratchDB + "Other_Sports_Facility").getOutput(0))>0: 
      if arcpy.Exists(outPath + "B405"):
        arcpy.Delete_management(outPath + "B405")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Other_Sports_Facility", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 405)
      rasTemp.save(outPath + "B405")
      arcpy.Delete_management(outPath + "tmpRaster")
  else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "B405", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
  rasterList_builtup.append(outPath + "B405")

  print ("Processing Tennis_Court ...")
  arcpy.Select_analysis("GREENSPACEBUILDINGS", scratchDB + "Tennis_Court", """"Code" = 406""")
  if int(arcpy.GetCount_management(scratchDB + "Tennis_Court").getOutput(0))>0: 
      if arcpy.Exists(outPath + "B406"):
        arcpy.Delete_management(outPath + "B406")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Tennis_Court", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 406)
      rasTemp.save(outPath + "B406")
      arcpy.Delete_management(outPath + "tmpRaster")
  else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "B406", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
  rasterList_builtup.append(outPath + "B406")

  print ("Processing Bowling_Green ...")
  arcpy.Select_analysis("GREENSPACEBUILDINGS", scratchDB + "Bowling_Green", """"Code" = 407""")
  if int(arcpy.GetCount_management(scratchDB + "Bowling_Green").getOutput(0))>0: 
      if arcpy.Exists(outPath + "B407"):
        arcpy.Delete_management(outPath + "B407")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Bowling_Green", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 407)
      rasTemp.save(outPath + "B407")
      arcpy.Delete_management(outPath + "tmpRaster")
  else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "B407", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
  rasterList_builtup.append(outPath + "B407")

  print ("Processing Golf_Course ...")
  arcpy.Select_analysis("GREENSPACEBUILDINGS", scratchDB + "Golf_Course", """"Code" = 408""")
  if int(arcpy.GetCount_management(scratchDB + "Golf_Course").getOutput(0))>0: 
      if arcpy.Exists(outPath + "B408"):
        arcpy.Delete_management(outPath + "B408")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Golf_Course", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 408)
      rasTemp.save(outPath + "B408")
      arcpy.Delete_management(outPath + "tmpRaster")
  else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "B408", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
  rasterList_builtup.append(outPath + "B408")

  print ("Processing Play_space ...")
  arcpy.Select_analysis("GREENSPACEBUILDINGS", scratchDB + "Play_space", """"Code" = 409""")
  if int(arcpy.GetCount_management(scratchDB + "Play_space").getOutput(0))>0: 
      if arcpy.Exists(outPath + "B409"):
        arcpy.Delete_management(outPath + "B409")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Play_space", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 409)
      rasTemp.save(outPath + "B409")
      arcpy.Delete_management(outPath + "tmpRaster")
  else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "B409", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
  rasterList_builtup.append(outPath + "B409")

  print ("Processing Religious_Grounds ...")
  arcpy.Select_analysis("GREENSPACEBUILDINGS", scratchDB + "Religious_Grounds", """"Code" = 410""")
  if int(arcpy.GetCount_management(scratchDB + "Religious_Grounds").getOutput(0))>0: 
      if arcpy.Exists(outPath + "B410"):
        arcpy.Delete_management(outPath + "B410")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Religious_Grounds", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 410)
      rasTemp.save(outPath + "B410")
      arcpy.Delete_management(outPath + "tmpRaster")
  else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "B410", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
  rasterList_builtup.append(outPath + "B410")

##GLASSHOUSE layer
  if GLASSHOUSE == 1:
    print ("Processing GLASSHOUSE ...")
    arcpy.Select_analysis("GLASSHOUSE", scratchDB + "Glasshouse", """"Code" = 404""")
    if int(arcpy.GetCount_management(scratchDB + "Glasshouse").getOutput(0))>0: 
      if arcpy.Exists(outPath + "B404"):
        arcpy.Delete_management(outPath + "B404")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Glasshouse", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 404)
      rasTemp.save(outPath + "B404")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "B404", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_builtup.append(outPath + "B404")

##in BUILDINGS layer
  if BUILDINGS == 1:
   print ("Processing BUILDINGSareas ...")
   arcpy.Select_analysis("BUILDINGS", scratchDB + "Buildings", """"Code" = 411""")
   if int(arcpy.GetCount_management(scratchDB + "Buildings").getOutput(0))>0: 
      if arcpy.Exists(outPath + "B411"):
        arcpy.Delete_management(outPath + "B411")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Buildings", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 411)
      rasTemp.save(outPath + "B411")
      arcpy.Delete_management(outPath + "tmpRaster")
   else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath +"B411", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")

## THEME NATURE (5)
                    
  rasterList_nature = []
                    
##  print ("Processing NATURE THEME ...")
  if NATURE_AREA == 1:
    print ("Processing NATURE_AREA ...")
    
    print ("Processing Broadleaf_woodland ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Broadleaf_woodland", """"code" = 501""")
    if int(arcpy.GetCount_management(scratchDB + "Broadleaf_woodland").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA501"):
        arcpy.Delete_management(outPath + "NA501")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Broadleaf_woodland", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 501)
      rasTemp.save(outPath + "NA501")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA501", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA501")

    print ("Processing Coniferous_woodland ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Coniferous_woodland", """"code" = 502""")
    if int(arcpy.GetCount_management(scratchDB + "Coniferous_woodland").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA502"):
        arcpy.Delete_management(outPath + "NA502")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Coniferous_woodland", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 502)
      rasTemp.save(outPath + "NA502")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA502", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA502")

    print ("Processing Improved_grassland ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Improved_grassland", """"code" = 503""")
    if int(arcpy.GetCount_management(scratchDB + "Improved_grassland").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA503"):
        arcpy.Delete_management(outPath + "NA503")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Improved_grassland", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 503)
      rasTemp.save(outPath + "NA503")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA503", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA503")
       
    print ("Processing Neutral_grassland ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Neutral_grassland", """"code" = 504""")
    if int(arcpy.GetCount_management(scratchDB + "Neutral_grassland").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA504"):
        arcpy.Delete_management(outPath + "NA504")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Neutral_grassland", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 504)
      rasTemp.save(outPath + "NA504")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA504", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA504")
       
    print ("Processing Calcareous_grassland ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Calcareous_grassland", """"code" = 505""")
    if int(arcpy.GetCount_management(scratchDB + "Calcareous_grassland").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA505"):
        arcpy.Delete_management(outPath + "NA505")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Calcareous_grassland", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 505)
      rasTemp.save(outPath + "NA505")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA505", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA505") 
       
    print ("Processing Acid_grassland ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Acid_grassland", """"code" = 506""")
    if int(arcpy.GetCount_management(scratchDB + "Acid_grassland").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA506"):
        arcpy.Delete_management(outPath + "NA506")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Acid_grassland", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 506)
      rasTemp.save(outPath + "NA506")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA506", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA506")
       
    print ("Processing Heather_grassland ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Heather_grassland", """"code" = 507""")
    if int(arcpy.GetCount_management(scratchDB + "Heather_grassland").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA507"):
        arcpy.Delete_management(outPath + "NA507")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Heather_grassland", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 507)
      rasTemp.save(outPath + "NA507")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA507", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA507") 
       
    print ("Processing Heather ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Heather", """"code" = 508""")
    if int(arcpy.GetCount_management(scratchDB + "Heather").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA508"):
        arcpy.Delete_management(outPath + "NA508")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Heather", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 508)
      rasTemp.save(outPath + "NA508")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA508", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA508")

    print ("Processing Inland_rock ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Inland_rock", """"code" = 509""")
    if int(arcpy.GetCount_management(scratchDB + "Inland_rock").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA509"):
        arcpy.Delete_management(outPath + "NA509")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Inland_rock", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 509)
      rasTemp.save(outPath + "NA509")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA509", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA509")                
           
    print ("Processing Supralittoral_rock ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Supralittoral_rock", """"code" = 510""")
    if int(arcpy.GetCount_management(scratchDB + "Supralittoral_rock").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA510"):
        arcpy.Delete_management(outPath + "NA510")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Supralittoral_rock", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 510)
      rasTemp.save(outPath + "NA510")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA510", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA510")
           
    print ("Processing Supralittoral_sediment ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Supralittoral_sediment", """"code" = 511""")
    if int(arcpy.GetCount_management(scratchDB + "Supralittoral_sediment").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA511"):
        arcpy.Delete_management(outPath + "NA511")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Supralittoral_sediment", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 511)
      rasTemp.save(outPath + "NA511")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA511", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA511")
           
    print ("Processing Littoral_rock ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Littoral_rock", """"code" = 512""")
    if int(arcpy.GetCount_management(scratchDB + "Littoral_rock").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA512"):
        arcpy.Delete_management(outPath + "NA512")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Littoral_rock", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 512)
      rasTemp.save(outPath + "NA512")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA512", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA512")
           
    print ("Processing Littoral_sediment ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Littoral_sediment", """"code" = 513""")
    if int(arcpy.GetCount_management(scratchDB + "Littoral_sediment").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA513"):
        arcpy.Delete_management(outPath + "NA513")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Littoral_sediment", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 513)
      rasTemp.save(outPath + "NA513")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA513", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA513")
           
    print ("Processing Fen_Marsh_Swamp ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Fen_Marsh_Swamp", """"code" = 514""")
    if int(arcpy.GetCount_management(scratchDB + "Fen_Marsh_Swamp").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA514"):
        arcpy.Delete_management(outPath + "NA514")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Fen_Marsh_Swamp", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 514)
      rasTemp.save(outPath + "NA514")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA514", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA514")
           
    print ("Processing Bog ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Bog", """"code" = 515""")
    if int(arcpy.GetCount_management(scratchDB + "Bog").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA515"):
        arcpy.Delete_management(outPath + "NA515")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Bog", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 515)
      rasTemp.save(outPath + "NA515")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA515", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA515")
           
    print ("Processing Saltmarsh ...")
    arcpy.Select_analysis("NATURE_AREA", scratchDB + "Saltmarsh", """"code" = 516""")
    if int(arcpy.GetCount_management(scratchDB + "Saltmarsh").getOutput(0))>0: 
      if arcpy.Exists(outPath + "NA516"):
        arcpy.Delete_management(outPath + "NA516")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Saltmarsh", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 516)
      rasTemp.save(outPath + "NA516")
      arcpy.Delete_management(outPath + "tmpRaster")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NA516", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NA516")

## NATURE_LINE layer
    
  print ("Processing NATURE_LINE ...")
  if NATURE_LINE == 1:
    print ("Processing NATURE_LINE ...")
    print ("Processing hedgerows ...")
    if int(arcpy.GetCount_management("NATURE_LINE").getOutput(0))>0:
      arcpy.MakeFeatureLayer_management("NATURE_LINE", "selectlyr")
    if arcpy.Exists(outPath + "NL517"):
      arcpy.Delete_management(outPath + "NL517")
      print ("... deleting existing raster")
    arcpy.Select_analysis("selectlyr", "hedgerows", """"code" = 517""")
    if int(arcpy.GetCount_management("hedgerows").getOutput(0))>0:
      print ("nowtime 1 is : "), time.strftime("%H:%M:%S", time.localtime())
      eucDistTemp = EucDistance("hedgerows","","1","")
      print ("nowtime 2 is : "), time.strftime("%H:%M:%S", time.localtime())
      rasTemp = Con(eucDistTemp < 1.5, 517, 1)
      rasTemp.save(outPath + "NL517")
    else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "NL517", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
    rasterList_nature.append(outPath + "NL517")

## THEME CULTIVABLE (6)
  print ("Processing CULTIVABLE THEME ...")
                    
  rasterList_cultivable = []
                    
#  print ("Processing Crops ...")
#  arcpy.Select_analysis("AGRICULTURECROPS", scratchDB + "Crops", """"code" = 610""")
#  if int(arcpy.GetCount_management(scratchDB + "Crops").getOutput(0))>0: 
#      if arcpy.Exists(outPath + "A610"):
#        arcpy.Delete_management(outPath + "A610")
#        print ("... deleting existing raster")
#      arcpy.PolygonToRaster_conversion(scratchDB + "Crops", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
#      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 610)
#      rasTemp.save(outPath + "A610")
#      arcpy.Delete_management(outPath + "tmpRaster")
#  else:
#      print (("No such features in the area"))
#      arcpy.CopyRaster_management(outPath + "landsea", outPath + "A610", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
#  rasterList_cultivable.append(outPath + "A610")
                                                   
  print ("Processing Arable_and_horticulture ...")
  arcpy.Select_analysis("AGRICULTUREARABLE", scratchDB + "Arable_and_horticulture", """"code" = 611""")
  if int(arcpy.GetCount_management(scratchDB + "Arable_and_horticulture").getOutput(0))>0: 
      if arcpy.Exists(outPath + "A611"):
        arcpy.Delete_management(outPath + "A611")
        print ("... deleting existing raster")
      arcpy.PolygonToRaster_conversion(scratchDB + "Arable_and_horticulture", "OBJECTID", outPath + "tmpRaster", "CELL_CENTER", "NONE", "1")
      rasTemp = Con(IsNull(outPath + "tmpRaster"), 1, 611)
      rasTemp.save(outPath + "A611")
      arcpy.Delete_management(outPath + "tmpRaster")
  else:
      print ("No such features in the area")
      arcpy.CopyRaster_management(outPath + "landsea", outPath + "A611", "DEFAULTS","0","9","","","8_BIT_UNSIGNED")
  rasterList_cultivable.append(outPath + "A611")

##in AGRICULTURERPA
##arable_land 1000+ A1000
  
  if LPIS_AND_CROPS_270122 == 1:
    
    print ("Processing LPIS_AND_CROPS_270122 ...")
    arcpy.MakeFeatureLayer_management("LPIS_AND_CROPS_270122", "fields1000_lyr")
    arcpy.SelectLayerByAttribute_management("fields1000_lyr", "NEW_SELECTION",)

    cursor = arcpy.UpdateCursor("fields1000_lyr")
    i = 1000
    for row in cursor:
      row.setValue('gridcode', i)
      i = i+1
      cursor.updateRow(row)

    arcpy.SelectLayerByAttribute_management("fields1000_lyr", "CLEAR_SELECTION")
    arcpy.CopyFeatures_management("fields1000_lyr", scratchDB + "fields1000")
    arcpy.FeatureToRaster_conversion(scratchDB + "fields1000", "gridcode", scratchDB + "fields1000_raster", 1)
    outCon = Con(IsNull(Raster(scratchDB + "fields1000_raster")), 1, Raster(scratchDB + "fields1000_raster"))
    outCon.save(outPath + "fields1000")

  rasTemp = ''
  rasTemp1 = ''
  print (" ")

# Processing arable land for script 2 data ()
  print ("Processing cadastral data ...")
  arcpy.Select_analysis("LPIS_AND_CROPS_270122", scratchDB + "crops", """"code" = 610""")
  arcpy.PolygonToRaster_conversion(scratchDB + "crops", "OBJECTID", scratchDB + "tmpRaster", "CELL_CENTER", "NONE", "1")
  #### Original - kept getting Error 000732: Input Raster does nt exist or is not supported.
  ##outCon = Con(IsNull(Raster(scratchDB + "")), 0, 1)
  ## New
  outCon = Con(IsNull(Raster(scratchDB + "tmpRaster")), 0, 1)
  
####in AGRICULTURERPA
####arable_land 1000+ A1000
##  
##  if AGRICULTURECROPS == 1:
##    
##    print ("Processing AGRICULTURECROPS ...")
##    arcpy.MakeFeatureLayer_management("AGRICULTURECROPS", "fields1000_lyr")
##    arcpy.SelectLayerByAttribute_management("fields1000_lyr", "NEW_SELECTION",)
##
##    cursor = arcpy.UpdateCursor("fields1000_lyr")
##    i = 1000
##    for row in cursor:
##      row.setValue('gridcode', i)
##      i = i+1
##      cursor.updateRow(row)
##
##    arcpy.SelectLayerByAttribute_management("fields1000_lyr", "CLEAR_SELECTION")
##    arcpy.CopyFeatures_management("fields1000_lyr", scratchDB + "fields1000")
##    arcpy.FeatureToRaster_conversion(scratchDB + "fields1000", "gridcode", scratchDB + "fields1000_raster", 1)
##    outCon = Con(IsNull(Raster(scratchDB + "fields1000_raster")), 1, Raster(scratchDB + "fields1000_raster"))
##    outCon.save(outPath + "fields1000")
##
##  rasTemp = ''
##  rasTemp1 = ''
##  print " "
##
### Processing arable land for script 2 data ()
##  print ("Processing cadastral data ...")
##  arcpy.Select_analysis("AGRICULTURECROPS", scratchDB + "crops", """"code" = 610""")
##  arcpy.PolygonToRaster_conversion(scratchDB + "crops", "OBJECTID", scratchDB + "tmpRaster", "CELL_CENTER", "NONE", "1")
##  outCon = Con(IsNull(Raster(scratchDB + "")), 0, 1)  
   
#===== End Chunk: Conversion =====#

#===== Chunk: Themes =====#
 #Combine rasters to thematic maps

  if WATER_AREA == 1:   #Assembles a water theme
    print ("Processing water layers ...")
    if arcpy.Exists(outPath + "T1_water"):
      arcpy.Delete_management(outPath + "T1_water")
      print ("... deleting existing raster")
    print (rasterList_water)
    rasTemp = CellStatistics(rasterList_water, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T1_water")

  if ROAD == 1:   #Assembles a road theme
    print ("Processing road layers ...")
    if arcpy.Exists(outPath + "T2_road"):
      arcpy.Delete_management(outPath + "T2_road")
      print ("... deleting existing raster")
    print (rasterList_transport)
    rasTemp = CellStatistics(rasterList_transport, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T2_road")

  if CULTURAL == 1:   #Assembles a cultural theme
    print ("Processing cultural layers ...")
    if arcpy.Exists(outPath + "T3_cultural"):
      arcpy.Delete_management(outPath + "T3_cultural")
      print ("... deleting existing raster")
    print (rasterList_cultural)
    rasTemp = CellStatistics(rasterList_cultural, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T3_cultural")

  if BUILDINGS == 1:   #Assembles a builtup theme
    print ("Processing builtup layers ...")
    if arcpy.Exists(outPath + "T4_builtup"):
      arcpy.Delete_management(outPath + "T4_builtup")
      print ("... deleting existing raster")
    print (rasterList_builtup)
    rasTemp = CellStatistics(rasterList_builtup, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T4_builtup")

  if NATURE_AREA == 1:   #Assembles a nature theme
    print ("Processing nature layers ...")
    if arcpy.Exists(outPath + "T5_nature"):
      arcpy.Delete_management(outPath + "T5_nature")
      print ("... deleting existing raster")
    print (rasterList_nature)
    rasTemp = CellStatistics(rasterList_nature, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T5_nature")

  if AGRICULTUREARABLE == 1:   #Assembles a cultivable theme
    print ("Processing cultivable layers ...")
    if arcpy.Exists(outPath + "T6_cultivable"):
      arcpy.Delete_management(outPath + "T6_cultivable")
      print ("... deleting existing raster")
    print (rasterList_cultivable)
    rasTemp = CellStatistics(rasterList_cultivable, "MAXIMUM", "DATA")
    rasTemp.save (outPath + "T6_cultivable")

##===== End Chunk: Themes =====#


#===== Chunk: Stack =====#
#Assemble the final map
#First delete any existing layers
  if finalmap == 1:   
    print ("Processing mosaic for all themes ...")
    if arcpy.Exists(outPath + "MapReclassified"):
      arcpy.Delete_management(outPath + "MapReclassified")
      print ("... deleting existing raster")
    if arcpy.Exists(outPath + "MapRaw"):
      arcpy.Delete_management(outPath + "MapRaw")
      print ("... deleting existing raster")
    if arcpy.Exists(outPath + "MapFinal"):
      arcpy.Delete_management(outPath + "MapFinal")
      print ("... deleting existing raster")

  print (" ")

 #Stack the thematic maps 
 #Here the hierarchy of individual themes is determined
  T1wa = Raster(outPath + "T1_water")
  T2ro = Raster(outPath + "T2_road")
  T3cu = Raster(outPath + "T3_cultural")
  T4bu = Raster(outPath + "T4_builtup")
  T5na = Raster(outPath + "T5_nature")
  T6cul = Raster(outPath + "T6_cultivable")
  field = Raster(outPath + "fields1000")   # fields
  buildings_B411 = Raster(outPath + "B411")
  


  step1 = Con(field > 999, field, 1)                    # fields first
  print ("fields added to map ...")
  step2 = Con(step1 == 1, T6cul, step1)                   # other cultivable areas on NOT (fields)
  print ("other cultivable areas added to map ...")                 
  step3 = Con(step2 == 1, T5na, step2)                  # natural areas on NOT (fields, cultivable)
  print ("natural areas added to map ...")
  step4 = Con(step3 == 1, T4bu, step3)                  # built up areas on NOT (fields, cultivable, natural areas)
  print ("built up areas added to map ...")
  step5 = Con(T3cu == 1, step4, T3cu)                  # cultural features on top
  print ("cultural landscape features added to map ...")
  step6 = Con(T1wa == 1, step5, T1wa)                   # freshwater on top
  print ("fresh water added to map ...")
  step7 = Con(T2ro == 1, step6, T2ro)                   # roads on top
  print ("roads added to map ...")
  map01 = Con(buildings_B411 == 1, step7, buildings_B411)     #buildings on top
  print ("buildings added to map...")
  map01.save (scratchDB + "MapRaw01")
  


  # Adding road verges to road pixels adjacent to fields or backround pixels         
  MapRaw_focal_max = FocalStatistics(scratchDB + "MapRaw01", NbrRectangle(3, 3, "CELL"), "MAXIMUM","")
  #MapRaw_focal_max.save (scratchDB + "MapRaw_focal_max") 
  MapRaw_focal_min = FocalStatistics(scratchDB + "MapRaw01", NbrRectangle(3, 3, "CELL"), "MINIMUM","")
  #MapRaw_focal_min.save (scratchDB + "MapRaw_focal_min")
  outCon1 = Con((MapRaw_focal_max >= 1000) | (MapRaw_focal_min == 1),1,0)
  outCon2 = Con((map01 >= 200) & (map01 <= 235),1,0)
  outCon = Con((outCon1 == 1) & (outCon2 == 1), 1, 0)
  map02 = Con(outCon == 1, 207, map01)
  map02.save (outPath + "MapRaw")

  nowTime = time.strftime('%X %x')
  print ("Raw landscape map created ...") + nowTime
  print ("  ")
#===== End Chunk: Stack =====#

except:
  if arcpy.Exists(outPath + "tmpRaster"):
      arcpy.Delete_management(outPath + "tmpRaster")
  tb = sys.exc_info()[2]
  tbinfo = traceback.format_tb(tb)[0]
  pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " +        str(sys.exc_type) + ": " + str(sys.exc_value) + "\n"
  msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

  arcpy.AddError(msgs)
  arcpy.AddError(pymsg)

  print (msgs)
  print (pymsg)

  arcpy.AddMessage(arcpy.GetMessages(1))
  print (arcpy.GetMessages(1))
