#=======================================================================================================================
### Name: Landscape generator for UK, part2
### Purpose: The script cleans up the raw landscape map from sliver polygons and creates ALMaSS inputs
### Authors: Elzbieta Ziolkowska - June 2017
### Last large update: January 2019
#Script modified for UK by Adam McVeigh - October 2020 - Janurary 2021

#===== Chunk: Setup =====#
# Import system modules
import arcpy, traceback, sys, time, gc, shutil, os, csv

#from dbfpy import dbf
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")
nowTime = time.strftime('%X %x')
gc.enable
print "Model landscape generator part2 started: " + nowTime
print "... system modules checked"

# Data - paths to data, output gdb, scratch folder and simulation landscape mask

template = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes/test_areas/test_area/" # Path to the template directory
dst = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes/test_areas/"  # Output destination main folder
#nationalDB = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes/data/UK_data.gdb/" # Folder with national data
nationalDB = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes/data/UK_data/" # Folder with national data

landscape = "StudyArea1" # Landscape name to process
landscape_mask = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes/data/" + landscape + ".shp"

# Data - paths to data, output gdb, scratch folder and simulation landscape mask

#test_areas = ["test_area2", "test_area3", "test_area4", "test_area5", "test_area6", "test_area7", "test_area8", "test_area10"]
test_areas = ["StudyArea1"]
for test_area in test_areas:
    outPath = os.path.join(dst, landscape, "outputs", landscape + ".gdb/")

#localSettings = project folder with mask
    out_feature = os.path.join(dst, landscape, "outputs/project.gdb/", landscape + "_mask")
    localSettings = out_feature

    gisDB = os.path.join(dst, landscape, landscape + ".gdb/")
    scratchDB = os.path.join(dst, landscape, "outputs/scratch.gdb/")  # scratch folder for shp tempfiles
    asciiexp = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes\ASCII_StudyArea1.txt"
    reclasstable = "E:/files_ALMaSS\ALMaSS_landscapes\UK_landscapes/reclasstable.txt"
    attrexp = "E:/files_ALMaSS/ALMaSS_landscapes/UK_landscapes\OutputStudyArea1\ATTR_StudyArea1.csv" 

    # Model settings
    arcpy.env.overwriteOutput = True
    arcpy.env.workspace = gisDB
    arcpy.env.scratchWorkspace = scratchDB
    arcpy.env.extent = localSettings
    arcpy.env.mask = localSettings
    arcpy.env.cellSize = localSettings
    print "... model settings read"

    # Defining dbf to csv conversion function
    def dbf_to_csv(out_table):#Input a dbf, output a csv
        csv_fn = out_table[:-4]+ ".csv" #Set the table as .csv format
        with open(csv_fn,'wb') as csvfile: #Create a csv file and write contents from dbf
            in_db = dbf.Dbf(out_table)
            out_csv = csv.writer(csvfile)
            names = []
            for field in in_db.header.fields: #Write headers
                names.append(field.name)
            out_csv.writerow(names)
            for rec in in_db: #Write records
                out_csv.writerow(rec.fieldData)
            in_db.close()
        return csv_fn

    ##### Setting a list with all the landscapes to process
    landscapes = ["StudyArea1"]

    print " "
    #===== End chunk: setup =====#

#####################################################################################################

### Processing each landscape
for index in range(len(landscapes)):
    print "Processing landscape " + landscapes[index] + " ..."

    #===== Chunk: Landscape-specific setup =====#

    arcpy.env.extent = localSettings
    arcpy.env.mask = localSettings

    # Data - paths to data, output gdb, scratch folder and simulation landscape mask
    outPath = os.path.join(dst, landscapes[index], "outputs", landscapes[index] + ".gdb/")
    gisDB = os.path.join(dst, landscapes[index], landscapes[index] + ".gdb/")
    scratchDB = os.path.join(dst, landscapes[index], "outputs/scratch.gdb/")  # scratch folder for tempfiles
    scratch = os.path.join(dst, landscapes[index], "outputs/scratch/")  # scratch folder for tempfiles
    #localSettings = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")   # project folder with mask
    localSettings = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")   # project folder with mask

    asciifile = "ASCII_" + landscapes[index] + ".txt"
    asciiexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", asciifile)  # export in ascii (for ALMaSS)
    attrtable = "ATTR_" + landscapes[index] + ".csv"  # Name of attribute table
    attrexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", attrtable)  # full path 
    soiltable = "SOIL_" + landscapes[index] + ".csv"  # Name of soil type table
    soilexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", soiltable)  # full path
    fieldstable = "FIELDS_" + landscapes[index] + ".dbf"  # Name of fields table
    fieldsexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files", fieldstable)  # full path
    permtable = "PERM_CROPS_" + landscapes[index] + ".csv"  # Name of permanent crops table
    permexp = os.path.join(dst, landscapes[index], "outputs/ALMaSS_files")  # folder

    # Deleting folder with ALMaSS outputs if exists
    if os.path.exists(os.path.join(dst, landscapes[index], "outputs/ALMaSS_files")):
        shutil.rmtree(os.path.join(dst, landscapes[index], "outputs/ALMaSS_files"))
    print "... deleting existing folder"
    os.mkdir(os.path.join(dst, landscapes[index], "outputs/ALMaSS_files"))

    # Model settings
    arcpy.env.overwriteOutput = True
    arcpy.env.workspace = gisDB
    arcpy.env.scratchWorkspace = scratchDB
    arcpy.env.extent = localSettings
    arcpy.env.mask = localSettings
    arcpy.env.cellSize = localSettings
    print "... model settings read"
    print " "

    #===== End chunk: Landscape-specific setup =====#

    try:

    #===== Chunk: Elimination of sliver polygons =====#
                 
        print "Eliminating sliver polygons ..."

        #step0: removing water from the process
        print "Step 0: Removing water from the process ..."
        map0_NW = SetNull(outPath + "MapRaw", outPath + "MapRaw", "VALUE IN (101, 102, 103, 104, 105, 106, 107)")
        map0_NW.save(scratchDB + "map0_NW")
        arcpy.RasterToPolygon_conversion(map0_NW, scratchDB + "map0_NW_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 0 finished ..."

        #step1: checking if any big background polygons (> 1ha) exists and if so convert them into fields
        print "Step 1: Checking for big (>1ha) background polygons ..."
        #arcpy.Select_analysis("AGRICULTURECROPS", scratchDB + "arable_land", "code = 610")
        arcpy.Select_analysis("LPIS_AND_CROPS_270122", scratchDB + "arable_land", "code = 610")        
        arcpy.PolygonToRaster_conversion(scratchDB + "arable_land", "OBJECTID", scratchDB + "tmpRaster", "CELL_CENTER", "NONE", "1")
        outCon = Con(IsNull(Raster(scratchDB + "tmpRaster")), 0, 1)
        
        map0_NW_MAXResult = arcpy.GetRasterProperties_management(scratchDB + "map0_NW", "MAXIMUM")
        map0_NW_MAX = map0_NW_MAXResult.getOutput(0)

        arcpy.MakeFeatureLayer_management(scratchDB + "map0_NW_vector", "map0_NW_lyr")
        arcpy.SelectLayerByAttribute_management("map0_NW_lyr", "NEW_SELECTION", "gridcode = 1")
        arcpy.CopyFeatures_management("map0_NW_lyr", scratchDB + "map0_NW_selection")
        arcpy.SelectLayerByAttribute_management("map0_NW_lyr", "SUBSET_SELECTION", "(Shape_Area >= 10000) AND (Shape_Area/Shape_Length>5)")
        arcpy.CopyFeatures_management("map0_NW_lyr", scratchDB + "map0_NW_selection2")

        if int(arcpy.GetCount_management(scratchDB + "map0_NW_selection2").getOutput(0))>0:
            ZonalStatisticsAsTable(scratchDB + "map0_NW_selection2", "Id", outCon, scratchDB + "map0_NW_table", "NODATA", "ALL")
            arcpy.JoinField_management (scratchDB + "map0_NW_vector", "Id", scratchDB + "map0_NW_table", "Id", ["MEAN"])
            cursor = arcpy.UpdateCursor(scratchDB + "map0_NW_vector")
            i = 1
            for row in cursor:
                if row.getValue('MEAN')>=0.8:
                    print int(map0_NW_MAX) + i
                    row.setValue('gridcode', int(map0_NW_MAX) + i)   # setting gridcode for a new field
                    i = i+1
                cursor.updateRow(row)

        # Updating field layer and adding info on farm type (all new fields belongs to one big farm of most common type 1)
        # arcpy.Update_analysis("AGRICULTURECROPS", scratchDB + "map0_NW_selection2", scratchDB + "FIELDS_" + landscapes[index] + "_with_missing_fields", "BORDERS")        
        # arcpy.Update_analysis("LPIS_AND_CROPS_270122", scratchDB + "map0_NW_selection2", scratchDB + "FIELDS_" + landscapes[index] + "_with_missing_fields", "BORDERS")
        # cursor=arcpy.SearchCursor(scratchDB + "FIELDS_" + landscapes[index] + "_with_missing_fields")
        # farmTypeList=[]
        # for row in cursor:
        #     farmTypeList.append(row.getValue("Farm_Type"))
        # farmTypeList[:] = (value for value in farmTypeList if value != 0)
        # farmTypeMajority = max(set(farmTypeList), key = farmTypeList.count)

        cursor = arcpy.UpdateCursor(scratchDB + "FIELDS_" + landscapes[index] + "_with_missing_fields")
        j=1
        #updating information in field layer
        for row in cursor:
            if ((row.getValue('Farm_Type')==0) and (row.getValue('IDNR')==0)): # updating only those fields with no infor on farm type and farmer number ('IDNR')
                row.setValue('IDENT_DZIALKI_EWIDENCYJNEJ', "added_field_no" + str(j))
                row.setValue('Farm_Type', farmTypeMajority)   # setting FarmType for a new field
                row.setValue('gridcode', int(map0_NW_MAX) + j)   # setting gridcode for a new field
                row.setValue('ALMaSScode', 20)   # setting gridcode for a new field
                j=j+1
            cursor.updateRow(row)

        arcpy.PolygonToRaster_conversion(scratchDB + "map0_NW_vector", "gridcode", scratchDB + "map1_NW", "", "", 1)
        arcpy.Delete_management(outPath + "tmpRaster")
        print "Step 1 finished ..."

        #step2: removing elongated background polygons
        print "Step 2: Removing elongated background polygons ..."
        arcpy.MakeFeatureLayer_management(scratchDB + "map0_NW_vector", "map1_NW_lyr")
        arcpy.SelectLayerByAttribute_management("map1_NW_lyr", "NEW_SELECTION", "gridcode = 1")
        arcpy.CopyFeatures_management("map1_NW_lyr", scratchDB + "map1_NW_selection")

        arcpy.MinimumBoundingGeometry_management(scratchDB + "map1_NW_selection", scratchDB + "map1_NW_selection_bounding", "CIRCLE", "NONE", "", "MBG_FIELDS")
        arcpy.JoinField_management(scratchDB + "map1_NW_selection", "Id", scratchDB + "map1_NW_selection_bounding", "Id", "")
        arcpy.MakeFeatureLayer_management(scratchDB + "map1_NW_selection", "map1_NW_selection_lyr")
        arcpy.SelectLayerByAttribute_management("map1_NW_selection_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 < 0.05 AND Shape_Area/ Shape_Length<4")
        arcpy.SelectLayerByAttribute_management("map1_NW_selection_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 < 0.05 AND Shape_Area/ Shape_Length<4 AND Shape_Area < 1500")
        arcpy.PolygonToRaster_conversion("map1_NW_selection_lyr", "gridcode", scratchDB + "map1_NW_selection_lyr_raster", "", "", 1)
        Reclass2 = Con(IsNull(scratchDB + "map1_NW_selection_lyr_raster"), 2, 1)
        toNibble2 = SetNull(Reclass2, 2, "Value = 1")
        map2_NW = Nibble(scratchDB + "map1_NW", toNibble2, "DATA_ONLY")
        map2_NW.save(scratchDB + "map2_NW")
        arcpy.RasterToPolygon_conversion(map2_NW, scratchDB + "map2_NW_vector", "NO_SIMPLIFY", "VALUE")

        print "Step 2 finished ..."

        #step3: removing small parts of multi-divided fields
        print "Step 3: Checking for small parts of multi-divided fields ..."
        cursor=arcpy.SearchCursor(scratchDB + "map0_NW_vector")
        fields_list=[]
        duplicates=[]
        for row in cursor:
            a=row.getValue("gridcode")
            if a>=1000:
                if a in fields_list:
                    duplicates.append(a)
                else:
                    fields_list.append(a)
              
        duplicates=list(dict.fromkeys(duplicates))

        if len(duplicates)>0:
            print "multi-divided fields found ..."
            whereClause = "VALUE IN (" + str(duplicates)[1:-1] + ")"
            toCheck3 = Con(scratchDB + "map2_NW", scratchDB + "map2_NW", 0, whereClause)
            regGr3 = RegionGroup(toCheck3, "FOUR", "WITHIN", "", 0)
            ##toNibble3 = SetNull(regGr3, 1, "COUNT < 1000")
            ##map3_NW = Nibble(scratchDB + "map2_NW", toNibble3, "DATA_ONLY")
            
            # new way without Nibble
            map3_NW = Con(regGr3, 570, scratchDB + "map2_NW", "COUNT < 1000")
            map3_NW.save(scratchDB + "map3_NW")
            arcpy.RasterToPolygon_conversion(map3_NW, scratchDB + "map3_NW_vector", "NO_SIMPLIFY", "VALUE")
            print "small multi-divided fields removed ..."    
        else:
            print "no multi-divided fields found ..."
            arcpy.CopyRaster_management(scratchDB + "map2_NW", scratchDB + "map3_NW")
            arcpy.CopyFeatures_management(scratchDB + "map2_NW_vector", scratchDB + "map3_NW_vector")
        print "Step 3 finished ..."

        #step4: adding the rivers back
        print "Step 4: Adding water ..."
        map4 = Con(IsNull(scratchDB + "map0_NW"), Raster(outPath + "MapRaw"), Raster(scratchDB + "map3_NW"))
        map4.save(scratchDB + "map4")
        # assigning riverside plants to all gaps, i.e., 'NODATA' values
        map4_fill = Con(IsNull(map4), 170, map4) # setting new category 'river side plants'
        map4_fill.save(scratchDB + "map4_fill")
        arcpy.RasterToPolygon_conversion(map4_fill, scratchDB + "map4_vector", "NO_SIMPLIFY", "VALUE")
        print "Step 4 finished ..."

        #step5: filling the background gaps
        print "Step 5: Filling the remaining gaps ..."
        gridcodeValues = []
        cursor = arcpy.SearchCursor(scratchDB + "map4_vector")
        for row in cursor:
            gridcodeValues.append(row.getValue('gridcode'))

        if 1 in gridcodeValues:
            print "Still some background pixels left ..."
            arcpy.PolygonNeighbors_analysis(scratchDB + "map4_vector", scratchDB + "map4_vector_neighbors")
            arcpy.JoinField_management (scratchDB + "map4_vector_neighbors", "src_OBJECTID", scratchDB + "map4_vector", "OBJECTID", ["gridcode"])
            arcpy.TableSelect_analysis(scratchDB + "map4_vector_neighbors", scratchDB + "map4_vector_neighbors_selection", "gridcode = 1")
            arcpy.JoinField_management (scratchDB + "map4_vector_neighbors_selection", "nbr_OBJECTID", scratchDB + "map4_vector", "OBJECTID", ["gridcode"])
            arcpy.TableSelect_analysis(scratchDB + "map4_vector_neighbors_selection", scratchDB + "map4_vector_neighbors_selection2", "gridcode_1 >= 411 AND gridcode_1 <= 472")
            arcpy.JoinField_management (scratchDB + "map4_vector", "OBJECTID", scratchDB + "map4_vector_neighbors_selection2", "src_OBJECTID")
            cursor = arcpy.UpdateCursor(scratchDB + "map4_vector")
            for row in cursor:
                if row.getValue('gridcode')==1:
                    if row.getValue('src_OBJECTID')>0:
                        row.setValue('gridcode', 440)   # setting category 'yards' to selected background objects
                    else:
                        row.setValue('gridcode', 570)   # setting category 'wasteland' to the rest of 'background' objects
                cursor.updateRow(row)
        else:
            print "No background pixels found ..."

            
        arcpy.PolygonToRaster_conversion(scratchDB + "map4_vector", "gridcode", scratchDB + "map5", "", "", 1)
        map5 = Raster(scratchDB + "map5")
        print "Step 5 finished ..."

        #step6: Cleaning of elongated sliver field polygons, e.g. left overs located between line of trees/hedgerow and road verge
        print "Step 6: Cleaning of elongated sliver field polygons ..."
        arcpy.MakeFeatureLayer_management(scratchDB + "map4_vector", "map4_lyr")
        arcpy.SelectLayerByAttribute_management("map4_lyr", "NEW_SELECTION", "gridcode >= 1000")
        arcpy.CopyFeatures_management("map4_lyr", scratchDB + "map4_selection")
        arcpy.MinimumBoundingGeometry_management(scratchDB + "map4_selection", scratchDB + "map4_selection_bounding", "CIRCLE", "NONE", "", "MBG_FIELDS")
        arcpy.JoinField_management(scratchDB + "map4_selection", "Id", scratchDB + "map4_selection_bounding", "Id", "")
        arcpy.MakeFeatureLayer_management(scratchDB + "map4_selection", "map4_selection_lyr")
        arcpy.SelectLayerByAttribute_management("map4_selection_lyr", "NEW_SELECTION", "Shape_Area/Shape_Area_1 < 0.05 AND Shape_Area/Shape_Length<4")
        arcpy.CopyFeatures_management("map4_selection_lyr", scratchDB + "map4_selection2")
        if int(arcpy.GetCount_management(scratchDB + "map4_selection2").getOutput(0))>0:
            arcpy.PolygonToRaster_conversion(scratchDB + "map4_selection2", "gridcode", scratchDB + "map4_selection2_raster", "", "", 1)
            #Reclass6 = Con(IsNull(scratchDB + "map4_selection2_raster"), 2, 1)
            #toNibble6 = SetNull(Reclass6, 2, "Value = 1")
            #map6 = Nibble(map5, toNibble6, "DATA_ONLY")
            # new way without Nibble
            map6 = Con(IsNull(scratchDB + "map4_selection2_raster"), scratchDB + "map5", 570) # setting category 'wasteland' to the elongated sliver field polygons
            arcpy.RasterToPolygon_conversion(map6, scratchDB + "map6_vector", "NO_SIMPLIFY", "VALUE")
        else:
            print "no cleaning needed ..."
            arcpy.CopyRaster_management(scratchDB + "map5", scratchDB + "map6")
            arcpy.CopyFeatures_management(scratchDB + "map4_vector", scratchDB + "map6_vector")
        print "Step 6 finished ..."

        cursor=arcpy.SearchCursor(scratchDB + "map6_vector")
        fields_list=[]
        duplicates_left=[]
        for row in cursor:
          a=row.getValue("gridcode")
          if a>=1000:
            if a in fields_list:
              duplicates_left.append(a)
            else:
              fields_list.append(a)


        arcpy.Dissolve_management(scratchDB + "map6_vector", scratchDB + "map6_vector_dissolve", ["gridcode"], "", "", "")
        arcpy.Update_analysis(scratchDB + "map6_vector", scratchDB + "map6_vector_dissolve", scratchDB + "map_final_vector", "BORDERS")
        arcpy.PolygonToRaster_conversion(scratchDB + "map_final_vector", "gridcode", outPath + "map_final", "", "", 1)
        print "Step 6 finished ..."
        nowTime = time.strftime('%X %x')
        print "Sliver polygons elimination done ..." + nowTime

    #===== End chunk: Elimination of sliver polygons =====#

  #save last map to output      

    #===== Chunk: Finalize =====#
        # Reclassify to ALMaSS raster values
        # ALMaSS uses different values for the landcover types, so this step simply translates the numeric values.
        reclass_land = ReclassByASCIIFile(outPath + "map_final", reclasstable, "DATA")
        reclass_land.save(outPath + "map_final_reclass")
        nowTime = time.strftime('%X %x')
        print "Reclassification done ..." + nowTime

        # clipping back to the original extent
        outExtractByMask = ExtractByMask(reclass_land, os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask"))
        outExtractByMask.save(outPath + "map_final_reclass_mask")

        arcpy.env.extent = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")
        arcpy.env.mask = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")
        arcpy.env.cellSize = os.path.join(dst, landscapes[index], "outputs/project.gdb", landscapes[index] + "_mask")

        # regionalise map
        regionALM = RegionGroup(outPath + "map_final_reclass_mask","EIGHT","WITHIN","ADD_LINK","")
        regionALM.save(outPath + "Map_ALMaSS")
        nowTime = time.strftime('%X %x')
        print "Regionalisation done ..." + nowTime

        # export attribute table 
        table = outPath + "Map_ALMaSS"
        # Write an attribute tabel - based on this answer:
        # https://geonet.esri.com/thread/83294
        # List the fields
        fields = arcpy.ListFields(table)  
        field_names = [field.name for field in fields]  
          
        with open(attrexp,'wb') as f:  
            w = csv.writer(f)  
            # Write the headers
            w.writerow(field_names)  
            # The search cursor iterates through the 
            for row in arcpy.SearchCursor(table):  
                field_vals = [row.getValue(field.name) for field in fields]  
                w.writerow(field_vals)  
                del row
        print "Attribute table exported..." + nowTime

        ##    # Find soil types

        # convert regionalised map to ascii
        arcpy.RasterToASCII_conversion(regionALM, asciiexp)
        print "Conversion to ASCII done ..." + nowTime

        # new version for fixing permanent crop staff


        print "Attribute table of permanent crops exported..." + nowTime

        endTime = time.strftime('%X %x')
        print ""
        print "Landscape generated: " + endTime
    #===== End Chunk: Finalize =====#

    except:
        if arcpy.Exists(outPath + "tmpRaster"):
          arcpy.Delete_management(outPath + "tmpRaster")
        tb = sys.exc_info()[2]
        tbinfo = traceback.format_tb(tb)[0]
        pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " +        str(sys.exc_type) + ": " + str(sys.exc_value) + "\n"
        msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

        arcpy.AddError(msgs)
        arcpy.AddError(pymsg)

        print msgs
        print pymsg

        arcpy.AddMessage(arcpy.GetMessages(1))
        print arcpy.GetMessages(1)

